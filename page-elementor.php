<?php
/**
 * Template Name: Page Elementor
 */
?>
<?php
  get_header();

  echo '<main class="main-elementor">';
    echo '<div class="container">';
      the_title( '<h1 class="page-title">', '</h1>' );
      the_content();
    echo '</div>';
  echo '</main>';
  
  get_footer();
?>