<?php
/**
 * Template Name: Page success
 */
?>
<?php
  get_header();

  $dir = 'template-parts/success/';

  echo '<main>';
    get_template_part( $dir . 'firstscreen' );
    get_template_part( $dir . 'download' );
    get_template_part( $dir . 'otzovik' );
  echo '</main>';
  
  get_footer();
?>