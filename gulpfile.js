const { src, dest, parallel, series, watch } = require( 'gulp' );
const browserSync  = require( 'browser-sync' ).create();
const concat       = require( 'gulp-concat' );
const sass         = require( 'gulp-sass' );
const autoprefixer = require( 'gulp-autoprefixer' );
const imagemin     = require( 'gulp-imagemin' );
const newer        = require( 'gulp-newer' );
const sourcemaps   = require( 'gulp-sourcemaps' );
const uglify       = require( 'gulp-uglify-es' ).default;
const cleancss     = require( 'gulp-clean-css' );

function browsersync() {
  browserSync.init({
    // server: { baseDir: './' }
    proxy: 'flagman.spb.wp',
		notify: false,
    online: true

  })
}

function styleDesktop() {
  return src( 'assets/sass/style.sass' )
  .pipe( sourcemaps.init() )
  .pipe( sass() )
  .pipe( concat( 'style.css' ) )
  .pipe( autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true }) )
  .pipe( sourcemaps.write('.') )
  .pipe( dest( './' ) )
  .pipe( browserSync.stream() )
}

function styleMobile() {
  return src( 'assets/sass/style-mobile.sass' )
  .pipe( sourcemaps.init() )
  .pipe( sass() )
  .pipe( concat( 'style-mobile.css' ) )
  .pipe( autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true }) )
  .pipe( sourcemaps.write('.') )
  .pipe( dest( './' ) )
  .pipe( browserSync.stream() )
}

function styleAddons() {
  return src( 'assets/sass/style-addons.sass' )
  .pipe( sourcemaps.init() )
  .pipe( sass() )
  .pipe( concat( 'style-addons.css' ) )
  .pipe( autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true }) )
  .pipe( sourcemaps.write('.') )
  .pipe( dest( './' ) )
  .pipe( browserSync.stream() )
}

function jsLibs() {
  return src( [
    'node_modules/slick-slider/slick/slick.min.js',
    // 'node_modules/rangeslider.js/dist/rangeslider.min.js',
    'node_modules/lazysizes/lazysizes.min.js',
    'node_modules/jquery-validation/dist/jquery.validate.min.js',
    'node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
    // 'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
    // 'node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js',
    // 'node_modules/flatpickr/dist/flatpickr.min.js',
    // 'node_modules/flatpickr/dist/l10n/ru.js'
  ] )
  .pipe( concat( 'libs.min.js' ) )
  .pipe( uglify() )
  .pipe( dest( 'assets/js/' ) )
  .pipe( browserSync.stream() )
}

function cssLibs() {
  return src( [
    'node_modules/slick-slider/slick/slick.css',
    'node_modules/slick-slider/slick/slick-theme.css',
    // 'node_modules/rangeslider.js/dist/rangeslider.css',
    // 'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.min.css',
    // 'node_modules/flatpickr/dist/flatpickr.min.css'
  ] )
  .pipe( concat( 'libs.min.css' ) )
  .pipe( autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true }) )
  .pipe( cleancss( { level: { 1: { specialComments: 0 } } } ) )
  .pipe( dest( 'assets/css/' ) )
  .pipe( browserSync.stream() )
}

function images() {
  return src( 'assets/images/**/*' )
  .pipe( newer( 'assets/img/' ) )
  .pipe( imagemin() )
  .pipe( dest( 'assets/img/' ) )
}

function startwatch() {
  watch( ['assets/sass/**/*.sass', '!assets/sass/**/*-mobile.sass', '!assets/sass/components-addons/*.sass'], styleDesktop );
  watch( ['assets/sass/**/*-mobile.sass', 'assets/sass/generic/**/*.sass', 'assets/sass/components/**/*.sass',
          '!assets/sass/components/**/*_media.sass', '!assets/sass/generic/**/*_media.sass', 
          '!assets/sass/components-addons/*.sass'], styleMobile );
  watch( ['assets/sass/style-addons.sass', 'assets/sass/components-addons/**/*.sass', 'assets/sass/media-addons/*.sass'], styleAddons );
  watch( './**/*.php' ).on( 'change', browserSync.reload );
  watch( 'assets/js/*.js' ).on( 'change', browserSync.reload );
  watch( 'assets/images/**/*', images );
}

exports.browsersync  = browsersync;
exports.styledesktop = styleDesktop;
exports.stylemobile  = styleMobile;
exports.styleaddons  = styleAddons;
exports.jsLibs       = jsLibs;
exports.cssLibs      = cssLibs
exports.imagesmin    = images;

exports.default = parallel( images, jsLibs, cssLibs, styleMobile, styleDesktop, styleAddons, browsersync, startwatch );