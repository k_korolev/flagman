<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package flagman
 */

?>
<?AmocrmRemontioForm::analyticsProlog();?>
<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
    <title>Ремонт квартир под ключ в Санкт-Петербурге и Ленинградской области — Флагман ремонта</title>
		<meta name="description" content="Профессиональный ремонт квартир под ключ в Санкт-Петербурге и Ленинградской области. Фиксированные сроки и стоимость, гарантия — 3 года!">
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?php wp_head(); ?>
		<?require_once(get_template_directory().'/assets/inc/analitycs_head.php');?>
	</head>

	<?php

		$user_device = user_device();
			
		if ( is_404() ) :
			$page_ID = '404'; // ID страницы не удалять!!!
		else :
			$page_ID = get_field('page_id'); // ID страницы не удалять!!!
		endif;

	?> 

	<body id="page-<?=$page_ID?>" <?php body_class(); ?> data-device="<?=$user_device?>">
	<?require_once(get_template_directory().'/assets/inc/analitycs_body.php');?>
	<?php
		wp_body_open();
		if ( is_page( array( 'privacy-policy' ) ) || is_page_template( 'page-success.php' ) || is_404() ) :
			get_template_part( 'template-parts/header-whithout_nav' );
		else :
			get_template_part( 'template-parts/header-main' );
		endif;
	?>
