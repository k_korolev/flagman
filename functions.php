<?php
/**
 * flagman functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package flagman
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/amocrm_remontio/functions.php';
include_once 'assets/libs/mobile_detect/Mobile_Detect.php';
include_once 'functions/our_repairs_func.php';
include_once 'functions/our_team_func.php';
include_once 'functions/partners_func.php';
include_once 'functions/modals.php';

function user_device() {
	$mobileDetect = new Mobile_Detect;
	if( $mobileDetect->isMobile() && !$mobileDetect->isTablet() ) {
		$user_device = 'mobile';
	} elseif( $mobileDetect->isTablet() ) {
		$user_device = 'tablet';
	} else {
		$user_device = 'desktop';
	}
	return $user_device;
}

function arr_render( $arr ) {
	echo '<pre>';
	print_r( $arr );
	echo '</pre>';
}

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'flagman_theme_setup' ) ) :
	
	function flagman_theme_setup() {
		
		load_theme_textdomain( 'flagman_theme', get_template_directory() . '/languages' );

		add_theme_support( 'post-thumbnails' );
		add_image_size( 'our_repairs-prv', 595, 405, true );
		add_image_size( 'our_repairs-nav', 185, 125, true );

		add_theme_support( 'customize-selective-refresh-widgets' );
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);
		add_theme_support(
			'custom-logo',
			array(
				'flex-width'  => false,
				'flex-height' => false,
				'header-text' => '',
				'unlink-homepage-logo' => false,
			)
		);

		register_nav_menus(
			array(
				'header' => esc_html__( 'Header', 'flagman_theme' ),
				'footer' => esc_html__( 'Footer', 'flagman_theme' ),
			)
		);

		add_action( 'init', 'create_blog_taxonomies' );
		function create_blog_taxonomies() {
						
			register_taxonomy('our_repairs_category', array('our_repairs'), array(
				'hierarchical'   => true, // Древовидная таксомтрия
				'labels'         => array(
					'name'              => 'Категории ремонта',
					'singular_name'     => 'Наши ремонты в категории',
					'search_items'      => 'Искать категории',
					'popular_items'     => 'Популярные категории',
					'all_items'         => 'Все категории',
					'parent_item'       => null,
					'parent_item_colon' => null,
					'edit_item'         => 'Редактировать категорию',
					'update_item'       => 'Обновить категорию',
					'add_new_item'      => 'Добавить новую категорию',
					'menu_name'         => 'Категории ремонта',
				),
				'public'            => true,
				'show_ui'           => true,
				'show_in_menu'      => true,
				'show_in_nav_menus' => true,
				'query_var'     		=> true,
				'show_in_rest'  		=> true,
				'meta_box_cb'   		=> 'post_categories_meta_box',
			));
						
			register_taxonomy('our_team_category', array('our_team'), array(
				'hierarchical'   => true, // Древовидная таксомтрия
				'labels'         => array(
					'name'              => 'Категории',
					'singular_name'     => 'Наша команда',
					'search_items'      => 'Искать категории',
					'popular_items'     => 'Популярные категории',
					'all_items'         => 'Все категории',
					'parent_item'       => null,
					'parent_item_colon' => null,
					'edit_item'         => 'Редактировать категорию',
					'update_item'       => 'Обновить категорию',
					'add_new_item'      => 'Добавить новую категорию',
					'menu_name'         => 'Категории',
				),
				'public'            => true,
				'show_ui'           => true,
				'show_in_menu'      => true,
				'show_in_nav_menus' => true,
				'query_var'     		=> true,
				'show_in_rest'  		=> true,
				'meta_box_cb'   		=> 'post_categories_meta_box',
			));
			
		}

		add_action( 'init', 'create_custom_posts' );
		function create_custom_posts() {
	
			register_post_type( 'pricing', array(
				'labels'              => array(
					'name'                 => 'Блок с ценами',
					'add_new'              => 'Добавить карточку',
					'all_items'            => 'Все карточки цен'
				),
				'public'              => true,
				'publicly_queryable'  => false,
				'exclude_from_search' => 0,
				'show_in'             => false,
				'show_in_rest'        => true,
				'query_var'           => true,
				'hierarchical'        => false,
				'menu_position'       => 21,
				'menu_icon'           => 'dashicons-money-alt',
				'capability_type'     => 'post',
				'supports'            => array( 'title' ),
				'rewrite'             => array('with_front' => false)
			));
	
			register_post_type( 'video_review', array(
				'labels'              => array(
					'name'                 => 'Видеоотзывы',
					'add_new'              => 'Добавить',
					'all_items'            => 'Все видеоотзывы'
				),
				'public'              => true,
				'publicly_queryable'  => false,
				'exclude_from_search' => 0,
				'show_in'             => false,
				'show_in_rest'        => true,
				'query_var'           => true,
				'hierarchical'        => false,
				'menu_position'       => 21,
				'menu_icon'           => 'dashicons-video-alt3',
				'capability_type'     => 'post',
				'supports'            => array( 'title' ),
				'rewrite'             => array('with_front' => false)
			));
	
			register_post_type( 'vlog', array(
				'labels'              => array(
					'name'                 => 'Vlog',
					'add_new'              => 'Добавить видео',
					'all_items'            => 'Все видео'
				),
				'public'              => true,
				'publicly_queryable'  => false,
				'exclude_from_search' => 0,
				'show_in'             => false,
				'show_in_rest'        => true,
				'query_var'           => true,
				'hierarchical'        => false,
				'menu_position'       => 21,
				'menu_icon'           => 'dashicons-video-alt3',
				'capability_type'     => 'post',
				'supports'            => array( 'title' ),
				'rewrite'             => array('with_front' => false)
			));
	
			register_post_type( 'partners', array(
				'labels'              => array(
					'name'                 => 'Партнеры',
					'add_new'              => 'Добавить',
					'all_items'            => 'Все партнеры'
				),
				'public'              => true,
				'publicly_queryable'  => false,
				'exclude_from_search' => 0,
				'show_in'             => false,
				'show_in_rest'        => true,
				'query_var'           => true,
				'hierarchical'        => false,
				'menu_position'       => 21,
				'menu_icon'           => 'dashicons-groups',
				'capability_type'     => 'post',
				'supports'            => array( 'title' ),
				'rewrite'             => array('with_front' => false)
			));
	
			register_post_type( 'our_team', array(
				'labels'              => array(
					'name'                 => 'Наша команда',
					'add_new'              => 'Добавить участника',
					'all_items'            => 'Вся команда'
				),
				'public'              => true,
				'publicly_queryable'  => false,
				'exclude_from_search' => 0,
				'show_in'             => false,
				'show_in_rest'        => true,
				'query_var'           => true,
				'hierarchical'        => false,
				'menu_position'       => 21,
				'menu_icon'           => 'dashicons-buddicons-buddypress-logo',
				'capability_type'     => 'post',
				'supports'            => array( 'title' ),
				'taxonomies'          => array( 'our_team_category' ),
				'rewrite'             => array('with_front' => false)
			));
	
			register_post_type( 'our_repairs', array(
				'labels'              => array(
					'name'                 => 'Наши ремонты',
					'add_new'              => 'Добавить объект',
					'all_items'            => 'Все ремонты'
				),
				'public'              => true,
				'publicly_queryable'  => false,
				'exclude_from_search' => 0,
				'show_in'             => false,
				'show_in_rest'        => true,
				'query_var'           => true,
				'hierarchical'        => false,
				'menu_position'       => 21,
				'menu_icon'           => 'dashicons-hammer',
				'capability_type'     => 'post',
				'supports'            => array( 'title', 'custom-fields' ),
				'taxonomies'          => array( 'our_repairs_category' ),
				'rewrite'             => array('with_front' => false)
			));
	
		}

	}
endif;
add_action( 'after_setup_theme', 'flagman_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function flagman_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'flagman_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'flagman_theme_content_width', 0 );

function flagman_theme_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'flagman_theme' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'flagman_theme' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'flagman_theme_widgets_init' );

function flagman_theme_styles() {
	if ( is_front_page() ) {
		wp_dequeue_style( 'wp-block-library' );
		wp_dequeue_style( 'wp-block-library-theme' );
	}
	if ( is_page_template( array( 'page-success.php', 'page-elementor.php' ) ) || is_404() ) {
		if ( is_page_template( 'page-success.php' ) ) {
			wp_enqueue_style( 'flagman_theme-datepicker', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css', array(), _S_VERSION );
		}
		wp_enqueue_style( 'flagman_theme-addons', get_template_directory_uri() . '/style-addons.css', array(), _S_VERSION );
	} else {
		wp_enqueue_style( 'flagman_theme-libs', get_template_directory_uri() . '/assets/css/libs.min.css', array(), _S_VERSION );
		if ( user_device() === 'mobile' ) {
			wp_enqueue_style( 'flagman_theme-style_mobile', get_template_directory_uri() . '/style-mobile.css', array(), _S_VERSION );
		} else {
			wp_enqueue_style( 'flagman_theme-style', get_stylesheet_uri(), array(), _S_VERSION );
		}
	}
	wp_enqueue_style( 'flagman_theme-additionals', get_template_directory_uri() . '/assets/css/additional.css', array(), _S_VERSION );
}
add_action( 'wp_enqueue_scripts', 'flagman_theme_styles' );

function flagman_theme_scripts() {

  wp_deregister_script( 'jquery' );
  wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js', false, null, true );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'flagman_theme-libs', get_template_directory_uri() . '/assets/js/libs.min.js', array(), null );
	if ( is_page_template( array( 'page-success.php', 'page-elementor.php' ) ) || is_404() ) {
		wp_enqueue_script( 'flagman_theme-datepicker', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js', array(), null );
		wp_enqueue_script( 'flagman_theme-datepicker-loc', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.ru.min.js', array(), null );
		wp_enqueue_script( 'flagman_theme-success', get_template_directory_uri() . '/assets/js/success.js', array(), null );
	} else {
		wp_enqueue_script( 'flagman_theme-script', get_template_directory_uri() . '/assets/js/common.js', array(), null );
	}
	
}
add_action( 'wp_footer', 'flagman_theme_scripts' );

function custom_script_loader_tag( $tag, $handle, $src ) {

	$scripts = array(
		'flagman_theme-script',
		'flagman_theme-success',
		'flagman_theme-libs',
		'flagman_theme-datepicker',
		'flagman_theme-datepicker-loc',
		'jquery',
	);
  
	foreach( $scripts as $defer_script ) {
		if ( $defer_script === $handle ) {
			if( $handle === 'flagman_theme-script' || $handle === 'flagman_theme-success' ) {
				$tag = '<script type="module" defer src="' . $src . '"></script>';
			} else {
				$tag = '<script defer src="' . $src . '"></script>';
			}			
		}
	}

  return $tag;
}
add_filter( 'script_loader_tag', 'custom_script_loader_tag', 10, 3 );

remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
remove_action( 'wp_head', 'wp_oembed_add_host_js' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
add_filter( 'emoji_svg_url', '__return_false' );

// Добавляем столбец "Категории работ" в "Наших ремонтах"
function true_add_post_columns($my_columns){
	$my_columns['our_repairs_category'] = 'Категория ремонта';
	return $my_columns;
}

function true_fill_post_columns( $column ) {
	global $post;
	$terms = get_the_terms($post->ID, 'our_repairs_category' );
	if ( $column === 'our_repairs_category' ) {
		echo '<a href="edit.php?our_repairs_category=' . $terms[0]->slug . '&post_type=our_repairs">' . $terms[0]->name . '</a>';
	}
}
 
add_filter( 'manage_edit-our_repairs_columns', 'true_add_post_columns', 10, 1 );
add_action( 'manage_posts_custom_column', 'true_fill_post_columns', 10, 1 );

// ......... //
