<?php
/**
 * Template Name: Page template
 */
?>
<?php
  get_header();

  echo '<main>';
    if ( is_page( 'kalkulyator-remonta' ) ) {
      get_template_part( 'template-parts/promo-detail' );
    } else {
      get_template_part( 'template-parts/promo' );
    }
    get_template_part( 'template-parts/portfolio' );
    get_template_part( 'template-parts/benefit' );
    get_template_part( 'template-parts/pricing' );
    get_template_part( 'template-parts/contract' );
    get_template_part( 'template-parts/our_team' );
    get_template_part( 'template-parts/gift' );
    get_template_part( 'template-parts/reviews' );
    get_template_part( 'template-parts/partners' );
    get_template_part( 'template-parts/vlog' );
    get_template_part( 'template-parts/contacts' );
  echo '</main>';
  
  get_footer();
?>
