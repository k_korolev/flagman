<?php
  add_action( 'wp_ajax_partners', 'partners' );
  add_action( 'wp_ajax_nopriv_partners', 'partners' );
  function partners() {
    
    $partners_args = [
      'numberposts' => 9,
      'post_type'   => 'partners',
      'order'       => 'ASC',
      'orderby'     => 'title',
      'suppress_filters' => true
    ];
    $partners_posts = get_posts( $partners_args );
    foreach ( $partners_posts as $post ) :

      setup_postdata( $post );

      $post_ID      = $post->ID; 
      $partner_desc = get_field( 'partner_desc', $post_ID );
      $partner_logo = get_field( 'partner_logo', $post_ID ); ?>

      <div class="partners_card">
        <div class="partners_card-body">
          <picture class="partners_card__pic"><img src="<?=$partner_logo?>" alt="img"></picture>
          <p class="partners_card__desc"><?=$partner_desc?></p>
        </div>
      </div>

    <? endforeach;
    wp_reset_postdata();
    wp_die();
    
  }