<?php
  add_action( 'wp_ajax_our_team', 'our_team' );
  add_action( 'wp_ajax_nopriv_our_team', 'our_team' );
  function our_team( $category_id ) {

    $category_id = $_POST['param'];
    $team_args = array(
      'numberposts' => 0,
      'post_type'   => 'our_team',
      'order'       => 'ASC',
      'orderby'     => 'title',
      'tax_query'   => array(
        array(
          'taxonomy' => 'our_team_category',
          'field'    => 'id',
          'terms'    => $category_id
        )
      ),
      'suppress_filters' => true
    );

    $team_posts = get_posts( $team_args );

    echo '<div class="our-team_carusel">';
    foreach ( $team_posts as $post ) :
      setup_postdata( $post );

      $post_ID = $post->ID;
      $member_name  = get_field( 'team_name', $post_ID );
      $member_post  = get_field( 'team_post', $post_ID );
      $member_photo = get_field( 'team_photo', $post_ID );

      ?>
      <div class="our-team_card" data-id="<?=$post_ID?>">
        <picture class="our-team_card__pic"><img data-lazy="<?=$member_photo?>" alt="<?=$member_name?>"></picture>
        <p class="our-team_card__name"><?=$member_name?></p>
        <p class="our-team_card__post"><?=$member_post?></p>
      </div>
      <?
      
    endforeach;
    wp_reset_postdata();
    echo '</div>';
    
    wp_die();
    
  }