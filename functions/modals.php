<?php
  
  add_action( 'wp_ajax_modal_pricing_info', 'modal_pricing_info' );
  add_action( 'wp_ajax_nopriv_modal_pricing_info', 'modal_pricing_info' );
  function modal_pricing_info() { 
      
    $card_ID = $_POST['pricing_id'];

    $pricing_works     = get_field_object( 'pricing_working', $card_ID );
    $pricing_materials = get_field_object( 'pricing_materials', $card_ID ); ?>
  
    <div class="modal-content">
      <button class="modal_close" data-toggle="modal-close"></button>
      <div class="modal-body">
        <h3 class="modal-pricing_title">Проводимые работы</h3>
        <article class="modal-pricing_info">
          <?php

            foreach ( $pricing_works['sub_fields'] as $card_works ) :
              // echo $pricing_works['value'][$card_works_slug];
              // echo '<pre>';
              // print_r($pricing_works);
              // echo '</pre>';
              if ( !empty( $card_works ) ) :
                $card_works_slug = $card_works['name'];
                $card_works_content = array_map('trim', explode(';', $pricing_works['value'][$card_works_slug])); ?>

                <div class="modal-pricing_info-wrap">
                  <p class="modal-pricing_info__title"><?=$card_works['label']?>:</p>
                  <ul class="modal-pricing_info__list">
                    <? foreach ( $card_works_content as $card_works_item ) :
                      echo '<li>' . $card_works_item . '</li>';
                    endforeach; ?>
                  </ul>
                </div>
              <? endif;
            endforeach;
            
            foreach ( array_reverse( $pricing_materials['sub_fields'] ) as $card_materials ) :
              if ( !empty( $card_materials ) ) :
                $card_materials_slug = $card_materials['name']; ?>

                <div class="modal-pricing_info-wrap">
                  <p class="modal-pricing_info__title"><?=$card_materials['label']?></p>
                  <p class="modal-pricing_info__desc"><?=$pricing_materials['value'][$card_materials_slug]?></p>
                </div>

              <? endif; 
            endforeach; ?>
        </article>
      </div>
    </div>

    <? wp_die();

  }

?>