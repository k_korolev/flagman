<?php
  add_action( 'wp_ajax_our_repairs', 'our_repairs' );
  add_action( 'wp_ajax_nopriv_our_repairs', 'our_repairs' );
  function our_repairs( $category_id ) {

    $category_slug = $_POST['param'];
    $order = $_POST['order'];
    $orderby = $_POST['orderby'];
    $max_page = get_term_by( 'slug', $category_slug, 'our_repairs_category' );
    $repairs_args = array(
      'numberposts' => 4,
      'post_type'   => 'our_repairs',
      'order'       => 'ASC',
      'orderby'     => $orderby,
      'tax_query'   => array(
        array(
          'taxonomy' => 'our_repairs_category',
          'field'    => 'slug',
          'terms'    => $category_slug
        )
      ),
      'suppress_filters' => true
    );
    
    echo '<div class="portfolio-content d-flex flex-wrap justify-content-center justify-content-lg-start" data-max="' . $max_page->count . '">';
    $repair_posts = get_posts( $repairs_args );
    foreach( $repair_posts as $post ) :

      setup_postdata( $post );
      $post_ID    = $post->ID;
      $post_title = get_field( 'our_repairs_title', $post_ID );
      $post_photo = get_field( 'our_repairs_photo', $post_ID ); ?>
      
      <div class="portfolio_card" data-id="<?=$post_ID?>">
        <p class="portfolio_card__title"><?=$post_title?></p>
        <picture class="portfolio_card__pic">
          <img class="lazyload" 
            data-src="<?=$post_photo[0]->guid?>" 
            src="<? echo get_template_directory_uri( ); ?>/assets/img/ajax-loader.gif" 
            alt="img">
        </picture>
        <div class="portfolio_card__carusel" data-slider="false">
          <?php foreach( $post_photo as $photo) :
              echo '<picture class="portfolio_card__carusel-item"><img data-lazy="' . $photo->guid . '" alt="img"></picture>';
          endforeach; ?>
        </div>
      </div> <?

    endforeach;
    echo '</div>';
    wp_reset_postdata();
    wp_die();
    
  }

  add_action( 'wp_ajax_our_repairs_loadmore', 'our_repairs_loadmore' );
  add_action( 'wp_ajax_nopriv_our_repairs_loadmore', 'our_repairs_loadmore' );
  function our_repairs_loadmore( $category_id ) {

    $items_id_arr = $_POST['items_id_arr'];
    $category_slug = $_POST['category_slug'];

    $repairs_args = array(
      'numberposts' => 4,
      'post_type'   => 'our_repairs',
      'exclude'     => $items_id_arr,
      'order'       => 'ASC',
      'tax_query'   => array(
        array(
          'taxonomy' => 'our_repairs_category',
          'field'    => 'slug',
          'terms'    => $category_slug
        )
      ),
      'suppress_filters' => true
    );
  
    $repair_posts = get_posts( $repairs_args );
    foreach( $repair_posts as $post ) :
  
      setup_postdata( $post );
      $post_ID    = $post->ID;
      $post_title = get_field( 'our_repairs_title', $post_ID );
      $post_photo = get_field( 'our_repairs_photo', $post_ID ); ?>
  
      <div class="portfolio_card" data-id="<?=$post_ID?>">
        <p class="portfolio_card__title"><?=$post_title?></p>
        <picture class="portfolio_card__pic">
          <img class="lazyload" 
            data-src="<?=$post_photo[0]->guid?>" 
            src="<? echo get_template_directory_uri( ); ?>/assets/img/ajax-loader.gif" 
            alt="img">
        </picture>
        <div class="portfolio_card__carusel" data-slider="false">
          <?php foreach( $post_photo as $photo) :
              echo '<picture class="portfolio_card__carusel-item"><img data-lazy="' . $photo->guid . '" alt="img"></picture>';
          endforeach; ?>
        </div>
      </div> <?
  
    endforeach;
    wp_reset_postdata();

    wp_die();
    
  }
  