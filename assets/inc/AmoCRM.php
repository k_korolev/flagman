<?

if (!function_exists('sendCalltouch')){
	function sendCalltouch($params = []){
		if( !$params || !$params['clientApiId'] ){
			return false;
		}
		$fio = urlencode($params['fio']);
		$orderComment = urlencode($params['orderComment']);
		$phoneNumber = $params['phoneNumber'];
		$clientApiId = $params['clientApiId'];
		if( $params['call_value'] ){
			$call_value = $params['call_value'];
		}else{
			$call_value = $_COOKIE['_ct_session_id'];
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded;charset=utf-8"));
		curl_setopt($ch, CURLOPT_URL, "http://api.calltouch.ru/calls-service/RestAPI/requests/orders/register/");
		curl_setopt($ch, CURLOPT_POST, 1);

		$caltouch_post = "clientApiId=".$clientApiId."&fio=".$fio."&phoneNumber=".$phoneNumber."&orderComment=".$orderComment."&".($call_value != 'undefined' ? "&sessionId=".$call_value : "");

		curl_setopt(
			$ch, 
			CURLOPT_POSTFIELDS,
			$caltouch_post
		);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$caltouch_response = curl_exec ($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$result = [
			"POST" => $caltouch_post,
			"HTTPCODE" => $httpcode,
			"RESPONSE" => $caltouch_response
		];

		if($params['log']){
			file_put_contents(
				$_SERVER['DOCUMENT_ROOT'].'/Nemocrm/Journal/calltouch_request.log', 
				var_export($result, true)."\n---------------------\n", 
				FILE_APPEND | LOCK_EX
			);
		}else{
			$result["RESPONSE"] = json_decode($result["RESPONSE"], true);
		}

		return $result;
	}
}

if (!function_exists('remontio_get_utm')){
	function remontio_get_utm(){
		session_start();

		if( trim($_SESSION['utm_source']) ){
		    $_POST['utm_source'] = $_SESSION['utm_source'];
		}
		if( trim($_SESSION['utm_medium']) ){
		    $_POST['utm_medium'] = $_SESSION['utm_medium'];
		}
		if( trim($_SESSION['utm_campaign']) ){
		    $_POST['utm_campaign'] = $_SESSION['utm_campaign'];
		}
		if( trim($_SESSION['utm_term']) ){
		    $_POST['utm_term'] = $_SESSION['utm_term'];
		}
		if( trim($_SESSION['utm_content']) ){
		    $_POST['utm_content'] = $_SESSION['utm_content'];
		}
		if( trim($_SESSION['keyword']) ){
		    $_POST['keyword'] = $_SESSION['keyword'];
		}	

		session_write_close();	
	}
}
if (!function_exists('get_ga_clientid')){
	function get_ga_clientid() {
	    if (isset($_COOKIE['_ga'])) {
	        list($version, $domainDepth, $cid1, $cid2) = preg_split('[\.]', $_COOKIE["_ga"], 4);
	        $contents = array(
	            'version' => $version,
	            'domainDepth' => $domainDepth,
	            'cid' => $cid1.'.'.$cid2
	        );
	        $cid = $contents['cid'];
	    } else {
	        $cid = '';
	    }
	    return $cid;
	}
}
if (!function_exists('sendMQLevent')){
	function sendMQLevent($remont_sq, $ga_id, $ga_clientid){
		unset($GLOBALS['mql']);
		if( $remont_sq > 25 ){
			// пример аналогичного js сниппета
			// (dataLayer = window.dataLayer || []).push({ 
			// 	'eCategory':'Lead', 
			// 	'eAction':'ContactUs', 
			// 	'eLabel':'MQL', 
			// 	'eNI':false, 
			// 	'event':'UAEvent' 
			// });
			$url = 'http://www.google-analytics.com/collect'
		    		.'?v=1'
		    		.'&tid='.$ga_id
		    		.'&cid='.$ga_clientid
		    		.'&t=event'
		    		.'&ec=Lead'
		    		.'&ea=ContactUs'
		    		.'&el=MQL';
		    		//.'&ni=0'

			$ch_ga = curl_init();

		    curl_setopt(
		    	$ch_ga, 
		    	CURLOPT_URL,
		    	$url
		    );
		    curl_setopt($ch_ga, CURLOPT_RETURNTRANSFER, true);
		    $ga_out = curl_exec($ch_ga);
		    curl_close($ch_ga);

		    /*
		    $ch_ga_debug = curl_init();
			$url_debug = 'https://www.remontexpress.ru/local/rest/debug/'
		    		.'?v=1'
		    		.'&tid='.$ga_id
		    		.'&cid='.$ga_clientid
		    		.'&t=event'
		    		.'&ec=Lead'
		    		.'&ea=ContactUs'
		    		.'&el=MQL'
		    		.'&referrer='.$_SERVER['SERVER_NAME'];
		    curl_setopt(
		    	$ch_ga_debug, 
		    	CURLOPT_URL,
		    	$url_debug
		    );
		    curl_setopt($ch_ga_debug, CURLOPT_RETURNTRANSFER, true);
		    $ga_out_debug = curl_exec($ch_ga_debug);
		    curl_close($ch_ga_debug);	

		    $GLOBALS['mql'] = 'y';
		    */
		    return true;		    	
		}
		return false;
		// return array('AREA'=>$remont_sq, 'GA_OUT'=>$ga_out, 'URL'=>$url);
	}
}
if (!function_exists('remontio_validate_phone')){
	function remontio_validate_phone($phone){
		// Allow +, - and . in phone number
		$filtered_phone_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
		// Remove "-" from number
		$phone_to_check = str_replace("-", "", $filtered_phone_number);

		// Check the lenght of number
		// This can be customized if you want phone number from a specific country	
		if (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14) {
			return false;
		} else {
			return true;
		}
	}
}
if (!function_exists('getCalcParams')){
	function getCalcParams($post){
		if( !$post['comment'] ){
			return $post;
		}

		$comment = $post['comment'];
		$calcParams = explode("\n", $comment);
		foreach( $calcParams as $k=>$param ){
		    $param = trim($param);
		    if( !$param ){
		        unset($calcParams[$k]);
		        continue;
		    }
		    if( ($k==2 || $k==3) && empty($post['remont_sq']) ){
		    	$param = removeFloatFromString($param);
		        $post['remont_sq'] = (int) filter_var($param, FILTER_SANITIZE_NUMBER_INT);
		        $post['remont_sq'] = abs($post['remont_sq']);
		    }
		    if( $k==5 && empty($post['estimated_cost']) ){

		    	if( preg_match("/от/", $param) && preg_match("/до/", $param) ){
		    		if( $prices = getStringBetween($param, 'от', 'до') ){
		    			$param = reset($prices);
		    		}
		    	}

		    	preg_match_all('!\d+!', $param, $matches);
		    	$matches = reset($matches);
		    	if( count($matches) > 1 && $matches[0] > 10000 ){
		    		$post['estimated_cost'] = reset($matches);
			    }else{
			    	$post['estimated_cost'] = (int) filter_var($param, FILTER_SANITIZE_NUMBER_INT);
			    }
		    }
		    if( preg_match("/осметически/", $param) && empty($post['remont_type']) ){
		        $post['remont_type'] = [
					"enum" => 721763,
					"value" => "Косметика"
				];
		    }
		    if( preg_match("/тандарт/", $param) && empty($post['remont_type']) ){
		        $post['remont_type'] = [
					"enum" => 721765,
					"value" => "Стандарт"
				];
		    } 
		    if( preg_match("/апитальны/", $param) && empty($post['remont_type']) ){
		        $post['remont_type'] = [
					"enum" => 721767,
					"value" => "Капиталка"
				];
		    } 
		    if( preg_match("/од ключ/", $param) && empty($post['remont_type']) ){
		        $post['remont_type'] = [
					"enum" => 1195046,				
					"value" => "Под ключ"
				];
		    }    	
		    if( preg_match("/изайнерски/", $param) && empty($post['remont_type']) ){
		        $post['remont_type'] = [
					"enum" => 721769,				
					"value" => "Дизайнерский"
				];
		    }
		    if( 
		        (preg_match("/торичка/", $param) ||
		        preg_match("/торичн/", $param) ||
		        preg_match("/овострой/", $param)) && empty($post['realty_type'])
		    ){
		        $post['realty_type'] = [
					"enum" => 721527,
					"value" => "Вторичка"
				];
		        if( preg_match("/овострой/", $param) ){
		            $post['realty_type'] = [
						"enum" => 721525,
						"value" => "Новостройка"
					];
		    		
		        }		
		    }
		    if( preg_match("/вартиры/", $param) && empty($post['room_type']) ){
		        $post['room_type'] = [
					"enum" => 579987,
					"value" => "Вся квартира"
				];
		    }    
		}

	    if( 
	        (preg_match("/торичка/", $post['comment']) ||
	        preg_match("/торичн/", $post['comment']) ||
	        preg_match("/овострой/", $post['comment'])) && empty($post['realty_type'])
	    ){
	        $post['realty_type'] = [
				"enum" => 721527,
				"value" => "Вторичка"
			];
	        if( preg_match("/овострой/", $post['comment']) ){
	            $post['realty_type'] = [
					"enum" => 721525,
					"value" => "Новостройка"
				];
	    		
	        }		
	    }

		return $post;
	}
}
if (!function_exists('getStringBetween')){
	function getStringBetween($source, $start = 'от', $end = 'до'){
		$result = [];
		if( preg_match("/$start/", $source) && preg_match("/$end/", $source) ){
			$modifySource = explode($start, $source);
			if( $modifySource[0] || $modifySource[1] ){
				if( trim($modifySource[1]) ){
					$modifySource = $modifySource[1];
				}else{
					$modifySource = $modifySource[0];
				}
			}else{
				$modifySource = $source;
			}

			$modifySource = explode($end, $modifySource);
			if( $modifySource[0] || $modifySource[1] ){
				$result[$start] = $modifySource[0];
				$result[$end] = $modifySource[1];
			}	
		}
		return $result;	
	}
}
if (!function_exists('removeFloatFromString')){
	function removeFloatFromString($param){
		if( strpos($param, '.') !== false ){ 
			$param = explode('.', $param);
			if( $param[0] ){
				$param = $param[0];
			}elseif( $param[1] ){
				$param = $param[1];
			}
		}
		if( strpos($param, ',') !== false ){
			$param = explode(',', $param);
			if( $param[0] ){
				$param = $param[0];
			}elseif( $param[1] ){
				$param = $param[1];
			}
		}
		return $param;
	}
}
function sendLeadToAmo(){
	remontio_get_utm(); // получить сохранённые метки
	$domain = $_SERVER['HTTP_HOST'] ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
	if(!$_POST['sait']){
		$_POST['sait'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
	}
	
	$comment_for_ga = $_POST['comment'];
	if ($_POST['referer'] == '') $_POST['referer'] = $_COOKIE['originreferer'];
	if( !$_POST['referer'] ){
		$_POST['referer'] = $_POST['1_referer'];
	}
	$_POST['key'] = "remexp03";
	$_POST['responsible'] = "2076463";
	
	$_POST['roistat'] = $_COOKIE['roistat_visit'];
	if (!$_POST['roistat']){
		$_POST['roistat'] = $_POST['roistat_visit'];
	}		
	if (!$_POST['roistat']){
		$_POST['roistat'] = $_POST['utm_source']." ".$_POST['utm_medium'];
	}
	if (!$_POST['roistat']){
		$_POST['roistat'] = $_POST['ya_google'];
	}

	$_POST['domain'] = $domain;
	$_POST['tag'] = $domain;
	$_POST['telefon_sdelki'] = preg_replace("/[^\d]/", "", $_POST['telefon_lida']);
	$_POST['leadname'] = $_POST['tag'].' - Заявка с сайта '.$_POST['telefon_sdelki'];
	$_POST['tag'] = [$_POST['tag'], 'новый обработчик'];

	$_POST['site_list'] = array(
		"enum" => 1213344,
		"value" => "ФР.спб"
	);
	$_POST['branch_office'] = ["enum" => 1168180, "value" => "Спб"];

	$_POST['call_site'] = array(
		"enum" => 1098819,
		"value" => "Заявка сайт"
	);

	if(preg_match("/ванная/iu", $_POST['comment'])){
		$_POST['room_type'] = array(
			"enum" => 579981,
			"value" => "Ванная"
		);
	}
	if(preg_match("/туалет/iu", $_POST['comment'])){
		$_POST['room_type'] = array(
			"enum" => 1057999,
			"value" => "Туалет"
		);
	}
	if(preg_match("/Совмещенный санузел/iu", $_POST['comment'])){
		$_POST['room_type'] = array(
			"enum" => 1058001,
			"value" => "С/У совмещенный"
		);
	}
	preg_match_all("/полу\s([\d]{1,})/u", $_POST['comment'], $c_matches, PREG_SET_ORDER, 0);
	if (count($c_matches)){
		$_POST['remont_sq'] = $c_matches[0][1];
	}

	if(isset($_POST['comment'])){
		$_POST = getCalcParams($_POST);
	}

	$_POST['comment'] = str_replace('/n','  ', $_POST['comment']);
	$_POST['utm'] = $_POST['utm_source']." ".$_POST['utm_medium']." ".$_POST['utm_campaign']." ".$_POST['utm_term']." ".$_POST['utm_content']." ".$_POST['utm_keyword'];

	$ga_lead_id = md5(uniqid(rand(), true));
	$_POST['GA'] = $ga_lead_id;
	$ga_clientid = get_ga_clientid();
	if( trim($ga_clientid) ){
		$_POST['ga_clientid'] = $ga_clientid;
	}else{
		$ga_clientid = $_POST['ga_clientid'];
	}

	if( !$_POST['fbc'] ){
		$_POST['fbc'] = $_COOKIE['_fbc'];
	}
	if( !$_POST['fbp'] ){
		$_POST['fbp'] = $_COOKIE['_fbp'];
	}
	if( !$_POST['ya_clientid'] ){
		$_POST['ya_clientid'] = $_COOKIE['_ym_uid'];
	}
	$_POST['ip_lida'] = $_SERVER['REMOTE_ADDR'];

	if( !trim($_POST['ima_kl']) ){
		$_POST['ima_kl'] = 'Гость';
	}

	$_POST['token'] = 'da323d847cf1cc48162d11f0422b4ec1';
	$_POST['data_source'] = ["enum" => 1174892, "value" => "Сайт"];
	// $_POST['present_as'] = ["enum" => 1153083, "value" => "Флагман Ремонта"];
	$_POST['present_as'] = "Флагман Ремонта";

	// if(
	//     $_SERVER['REQUEST_METHOD'] === 'POST' || 
	//     // $_SERVER['REQUEST_METHOD'] === 'GET' ||
	//     $_SERVER['REQUEST_METHOD'] === 'PUT'
	// ){
	// 	if( !remontio_validate_phone($_POST['telefon_lida']) ){
	// 		$_POST['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
	// 		$_POST['get_browser'] = get_browser(null, true);
	// 	    file_put_contents(
	// 	    	$_SERVER['DOCUMENT_ROOT'].'/Nemocrm/Journal/spam.log', 
	// 	    	var_export(array(
	// 	    		"DATETIME" => date("Y-m-d H:i:s"),
	// 	    		"POST" => $_POST,
	// 	    	), true)."\n---------------------\n", 
	// 	    	FILE_APPEND | LOCK_EX
	// 	    );	
	//     }	
	// }

	/*******************Сохранение формы - старт************/
	require_once($_SERVER['DOCUMENT_ROOT'].'/Nemocrm/loader.php');
	$amoLeadSaver = new \Nemocrm\Save();
	$amoLeadSaverID = $amoLeadSaver->saveLead(serialize($_POST)); // сохраняем форму	
	/******************************************************/

	if( $_POST['ima_kl'] != 'nemocrm' ){
		$ch_amo = curl_init();
		// $request_url = "https://nemocrm.ru/api/integration/site/index.php";
		$request_url = "https://toamo.remont.io/request";
		curl_setopt($ch_amo, CURLOPT_URL, $request_url);
		curl_setopt($ch_amo, CURLOPT_POST, 1);
		curl_setopt($ch_amo, CURLOPT_POSTFIELDS, http_build_query($_POST));
		curl_setopt($ch_amo, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch_amo, CURLOPT_CONNECTTIMEOUT, 0); 
		curl_setopt($ch_amo, CURLOPT_TIMEOUT, 10); //timeout in seconds	

		$json = curl_exec($ch_amo);

		$httpcode = curl_getinfo($ch_amo, CURLINFO_HTTP_CODE);

		if( $httpcode != '200' ){
			// хост упал или повис
		}

		$response = json_decode($json, true);

		$lifetime = time()+60*10; // 10 минут

		if ((int)$response['lead']){
			setcookie('sendlead', $response['lead'], $lifetime, '/');
		}

		if ( $response['contact'] ){
			if ( is_array($response['contact']) ){
				$response['contact'] = $response['contact']['_embedded']['items'][0]['id'];
				if( (int)$response['lead'] && !$response['contact'] ){
					$response['contact'] = 777;
				}
			}
			if ((int)$response['contact']){
				setcookie('sendcontact', $response['contact'], $lifetime, '/');
			}
		}

		// сохраняем в сессию факт заявки
		session_start();
		$_SESSION['form_send'] = 'Y';
		session_write_close(); 

		curl_close($ch_amo);
	}

    file_put_contents(
    	$_SERVER['DOCUMENT_ROOT'].'/Nemocrm/Journal/toamo.remont.io', 
    	var_export(array(
    		"TIME" => date("Y-m-d H:i:s"),
    		"URL" => $request_url,
    		"POST" => $_POST,
    		"CODE" => $httpcode,
    		"RESP" => $json,
    		"RESP_FORMAT" => $response,
    	), true)."\n---------------------\n", 
    	FILE_APPEND | LOCK_EX
    );

	if( $_POST['estimated_cost'] ){
		setcookie('estimated_cost', $_POST['estimated_cost'], time() + 60 * 10);
	}

	/************Нотификация и журналирование***************/
	require_once($_SERVER['DOCUMENT_ROOT'].'/Nemocrm/loader.php');
	\Nemocrm\Nemo::Check(
		$json,
		$_POST,
		'info@flagmanremonta.ru',
		['info@flagmanremonta.ru']
	);
	/******************************************************/
	/*******************Сохранение формы - финал***********/
	if( (int)$response['lead'] ){ // unset($amoLeadSaver);
		$amoLeadSaver->updateLead($amoLeadSaverID, $response['lead'], $response['contact']); // добавляем id лида
		sendCalltouch([
			'clientApiId' => 'ePnn7m9LZl8KHrOSYPl0sSdtjy.SRwoOdzVWWP7ulo1az',
			'fio' => $_POST['ima_kl'],
			'phoneNumber' => $_POST['telefon_sdelki'],
			'orderComment' => $_POST['comment'],
			'log' => true
		]);			
	}
	unset($amoLeadSaver);
	/******************************************************/

	$mql = false;

	if( (int)$response['lead'] && $ga_lead_id && $ga_clientid && $ch_ga = curl_init() ) {
		$ga_id = 'UA-84888207-1';
		$mql = sendMQLevent($_POST['remont_sq'], $ga_id, $ga_clientid); // пуш специальной цели в GA
	    curl_setopt($ch_ga, CURLOPT_URL, 'http://www.google-analytics.com/collect?v=1&tid='.$ga_id.'&cid='.$ga_clientid.'&t=event&ec=lead&ea=contact&el='.$ga_lead_id.'&ni=1');
	    curl_setopt($ch_ga, CURLOPT_RETURNTRANSFER,true);
	    $ga_out = curl_exec($ch_ga);
	    curl_close($ch_ga);
	}

	return $mql;
}