'use strict';

import headerBlock from './modules/header.js';
import ourRepairsBlock from './modules/ourRepairs.js';
import ourTeamBlock from './modules/ourTeam.js';
import anchorTarget from './modules/anchorTarget.js';
import { modalOpenAjax, modalOpen, modalClose } from './modules/modalActions.js';
import partnersBlock from './modules/partnersBlock.js';
import validate from './modules/validate.js';
import analitics from './modules/analitics.js';
import videoLoader from './modules/videoLoader.js';
import mapLoader from './modules/mapLoader.js';
import inputNumberFocus from './modules/inputNumerFocus.js';


const bodyTag = $('body');
const userDevice = bodyTag.attr('data-device');

function benefitBlock() {
    $('.benefit_select').on('change', () => $(`.benefit_card`).fadeToggle());
}

function pricingBlock() {
    $('.pricing-content').slick({
        mobileFirst: true,
        dots: true,
        arrows: false,
        adaptiveHeight: true,
        responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 1365,
                settings: 'unslick'
            }
        ]
    });
}

function maskInit() {
    $.mask.definitions.q = "[1,2,3,4,5,6,9]";
    $(this).mask('+7 (q99) 999-99-99');
}

$(document).ready(() => {
    headerBlock();
    inputNumberFocus();
    ourRepairsBlock(userDevice);
    ourTeamBlock(userDevice);
    benefitBlock();
    pricingBlock();
    partnersBlock();
    videoLoader('.vlog_card__pic, .reviews_card__pic');
    mapLoader();
    analitics();

    $(document).on('click', 'button[data-toggle="modal-open"]', modalOpen);
    $(document).on('click', 'button[data-toggle="modal-open-ajax"]', modalOpenAjax);
    $(document).on('click', 'button[data-toggle="modal-close"]', modalClose);
    $(document).on('focusin', 'input[name="telefon_lida"]', maskInit);
    $(document).on('click', 'form [type="submit"]', validate);

    anchorTarget('.btn-top a');
    anchorTarget('.menu-item a');
});