'use strict';

import headerBlock from './modules/header.js';
import anchorTarget from './modules/anchorTarget.js';
import validate from './modules/validate.js';
import { getCookie } from './modules/cookie.js';

function dateBlock() {
    $('.firstscreen input[name="date"]').datepicker({
        format: "dd/mm/yyyy",
        startDate: "today",
        language: "ru",
        orientation: "bottom auto",
        todayHighlight: true,
        toggleActive: true,
        autoclose: true
    });

    $('.firstscreen input[name="time"]').on('click', () => $('.section-board_time__list').toggleClass('show'));
    $('.section-board_time__list li').on('click', function() {
        const curTime = $(this).text();
        $('.firstscreen input[name="time"]').val(curTime);
        $('.section-board_time__list').toggleClass('show');
    });

}

function currentHomePage() {
    const pageID = $(document.body).attr('id').slice(5);
    console.log(pageID)
    if (pageID === 'success_detail') {
        const homePageLink = getCookie('home_page_link');
        console.log(homePageLink)
        $('.custom-logo-link, .footer_logo').attr('href', homePageLink);
    }
}

$(document).ready(() => {
    headerBlock();
    dateBlock();
    currentHomePage();

    $(document).on('click', 'form [type="submit"]', validate);

    anchorTarget('.btn-top a');
});