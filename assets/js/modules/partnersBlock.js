import scrollLoader from './scrollLoader.js';

const partnersBlock = () => {

  const options = {
    blockClass: '.partners',
    ajaxLoader: true,
    action: 'partners',
    param: '',
    handler: (response) => {
      $('.partners-content').html(response);

      $('.partners-content').slick({
        mobileFirst: true,
        dots: true,
        arrows: false,
        adaptiveHeight: true,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
          {
            breakpoint: 1199,
            settings: 'unslick'
          }
        ]
      });

    }
  }

  scrollLoader(options);
  
}


export default partnersBlock;