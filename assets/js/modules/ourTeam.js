import ajaxContentLoader from './ajaxContentLoader.js';
import scrollLoader from './scrollLoader.js';

const blockClass = '.our-team';
const ajaxAction = 'our_team';
const param = 'param=';

const ourTeamBlock = ( userDevice ) => {

  let curValue;
  const nav = $(`${blockClass}_nav`),
        navItem = nav.children();
  const preload = {
    beforeSend: () => {
      $('.our-team-content').addClass('opacity-loading').removeClass('opacity-loaded');
    },
    complete: () => {
      $('.our-team-content').removeClass('opacity-loading').addClass('opacity-loaded');
    }
  }

  if ( userDevice === 'mobile' ) {
    curValue = nav.first().val();
    nav.on('change', () => {
      curValue = nav.val();
      ajaxContentLoader( ajaxAction, param + curValue, succesHandler, preload );
    });
  } else {
    curValue = navItem.first().attr('data-value');
    navItem.first().addClass('active');
    navItem.on('click', function() {
      curValue = $(this).attr('data-value');
      ajaxContentLoader( ajaxAction, param + curValue, succesHandler, preload );
      $(this).addClass('active').siblings().removeClass('active');
    });
  }

  scrollLoader({
    ajaxLoader: true,
    blockClass: blockClass,
    action: ajaxAction,
    param: curValue,
    handler: succesHandler
  });

}

const succesHandler = ( response ) => {
  $(`${blockClass}-content`).html(response);
  $(`${blockClass}_carusel`).slick({
    mobileFirst: true,
    dots: true,
    arrows: false,
    lazyLoad: 'progressive',
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 1365,
        settings: {
          slidesToShow: 3,
          arrows: true,
          prevArrow: '<button class="slick-arrow slick-arrow-prev"></button>',
          nextArrow: '<button class="slick-arrow slick-arrow-next"></button>',
          dots: false
        }
      }
    ]
  });
}

export default ourTeamBlock;
