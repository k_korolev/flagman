const inputNumberFocus = () => {
  $('input[type="number"]').on('focusin', function() {
		$(this).val('');
		$(this).attr('min', '');
	});

	$('input[type="number"]').on('focusout', function() {
		const inputValue = $(this).val();
		if(inputValue < 10) {
			$(this).val(10);
		} else if(inputValue > 250) {
			$(this).val(250);
		}
	});
  
}

export default inputNumberFocus;