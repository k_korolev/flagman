const headerBlock = () => {
  const header = $('.header')
  const burger = $('.header_burger');
  const headerNav = $('.header_nav');

  burger.on('click', () => {
    burger.toggleClass('open');
    headerNav.slideToggle();
    headerNav.addClass('box-shadow');
  });

  if( header.offset().top > 0 ) {
    header.addClass('box-shadow');
  }

  $(window).on('scroll', () => {
    if($(window).scrollTop() > 0) {
      header.addClass('box-shadow');
    } else {
      header.removeClass('box-shadow');
    }
    });
}

export default headerBlock;