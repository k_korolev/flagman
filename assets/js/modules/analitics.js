import sendData from "./sendData.js";

const analitics = () => {

  // Navigation

	$(document).on('click', 'header .menu-item', function() {
		const regexp  = /label_(\S+)/ig
		const classes = $(this).attr('class');
		const label   = regexp.exec(classes)[1];
		sendData( 'navigation', 'click', 'menu.ssylka-'+label+'_click', 'true' );
	});

	// Header button

	$(document).on('click', '.header_nav__btn', () => sendData( 'form', 'click', 'menu.knopka-zakazat-zvonok_click', 'true' ));

	// First Screen

	$(document).on('click', '.promo .section-board_card__label, .promo .section-board_card__area', () => {
		sendData( 'navigation', 'click', '1ekran.kalkulyator_vzaimodeystvie', 'true' );
	});

	$(document).on('click', '.promo .btn', () => sendData( 'form', 'click', '1ekran.kalkulyator-knopka-uznat-stoimost_click', 'true' ));

	// Portfolio

	$(document).on('click', '.btn-list li', function() {
		const label = $(this).attr('data-value');
		sendData( 'navigation', 'click', 'portfolio.'+label, 'true' );
	});

	$(document).on('change', '.portfolio select.btn-list', function() {
		const label = $(this).val();
		sendData( 'navigation', 'click', 'portfolio.'+label, 'true' );
	});

  // Pricing

	$(document).on('click', '.pricing_card__btn', function() {
		const label = $(this).parents('.pricing_card').attr('data-label');
		sendData( 'form', 'click', 'tarif.'+label+'-knopka-uznat-stoimost_click', 'true' );
	});

	// Reviews

	$(document).on('click', '.reviews_card', () => sendData( 'navigation', 'click', 'otzyvy.video-preview_click', 'true' ));

	// Button to top

	$(document).on('click', '.btn-top a', () => sendData( 'navigation', 'click', 'podval.v-nachalo-str_click', 'true' ));

}

export default analitics;