import ajaxContentLoader from './ajaxContentLoader.js';

const loadMore = $('.portfolio button[data-toggle="load_more"]');
const nav = $('.portfolio-nav');

const ourRepairsBlock = (userDevice) => {

  let curVal;
  const navItem = nav.children();
  const preload = {
    beforeSend: () => {
      $('.portfolio-content').addClass('opacity-loading').removeClass('opacity-loaded');
    },
    complete: () => {
      $('.portfolio-content').removeClass('opacity-loading').addClass('opacity-loaded');
    }
  }

  if( userDevice === 'mobile' ) {
    curVal = nav.val();
    nav.on( 'change', () => {
      curVal = nav.val();
      ajaxContentLoader('our_repairs', 'param='+curVal, handler, preload );
    });
  } else {
    curVal = navItem.first().attr('data-value');
    navItem.first().addClass('active');
    navItem.on('click', function() {
      curVal = $(this).attr('data-value');
      ajaxContentLoader('our_repairs', 'param='+curVal, handler, preload );
      $(this).addClass('active').siblings().removeClass('active');
    });
  }

  ajaxContentLoader('our_repairs', 'param='+curVal, handler );
  loadMoreAction(userDevice);
  
}

const handler = (response) => {
  $('.portfolio-content').remove();
  $('.portfolio-wrap').prepend(response);
  $('.portfolio_card__carusel').each(function() {

    const _thisCarusel = $(this);

    if( _thisCarusel.find('picture').length > 3 ) {
      _thisCarusel.slick({
        mobileFirst: true,
        dots: false,
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        lazyLoad: 'progressive'
      });
  
      _thisCarusel.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        const nextPhotoScr = $(this).find(`.slick-slide[data-slick-index="${nextSlide}"] img`).attr('src');
        $(this).parent().find('.portfolio_card__pic img').attr( 'src', nextPhotoScr );
      });
  
      $('.slick-slide').on('click', function() {
        const slideIndex = $(this).attr('data-slick-index');
        const thisPhotoSrc = $(this).find('.portfolio_card__carusel-item img').attr('src');
  
        $(this).parents('.portfolio_card__carusel').slick('slickGoTo', slideIndex);
        $(this).parents('.portfolio_card').find('.portfolio_card__pic img').attr( 'src', thisPhotoSrc );
  
      });

      _thisCarusel.attr('data-slider', 'true');
    } else {
      $('.portfolio_card__carusel-item').on('click', function() {
        const thisPhotoSrc = $(this).find('img').attr('src');
        $(this).parents('.portfolio_card').find('.portfolio_card__pic img').attr( 'src', thisPhotoSrc );
      });
    }

  });
  loadMoreCheck();
}

const loadMoreCheck = () => {

  if ( $('.portfolio-content').attr('data-max') < 5 ) {
    loadMore.hide();
  } else {
    loadMore.show()
  }

}

const loadMoreAction = (userDevice) => {

  loadMore.on('click', () => {
    let itemsArr = [];
    let curCategory;

    // Определяем активную категорию
    if( userDevice === 'mobile' ) {
      curCategory = nav.val();
    } else {
      curCategory = $('.portfolio-nav li.active').attr('data-value');
    }
  
    // Собираем ID показаных постов в массив
    $('.portfolio_card').each(function() {
      let itemId = $(this).attr('data-id');
      itemsArr.push(itemId);
    });

    // Определяем кол-во оставшихся постов
    const postsCount = $('.portfolio-content').attr('data-max');
    const remainingPosts = postsCount - itemsArr.length;
    
    if ( remainingPosts < 5 ) {
      loadMore.hide();
    }
    
    const param = $.param({
      items_id_arr: itemsArr,
      category_slug: curCategory
    });
  
    ajaxContentLoader('our_repairs_loadmore', param, (data) => {
      $('.portfolio-content').append(data);
      $('.portfolio_card__carusel[data-slider="false"]').each(function() {
    
        const _thisCarusel = $(this);
    
        if( _thisCarusel.find('picture').length > 3 ) {
          _thisCarusel.slick({
            mobileFirst: true,
            dots: false,
            arrows: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            lazyLoad: 'progressive'
          });
      
          _thisCarusel.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            const nextPhotoScr = $(this).find(`.slick-slide[data-slick-index="${nextSlide}"] img`).attr('src');
            $(this).parent().find('.portfolio_card__pic img').attr( 'src', nextPhotoScr );
          });
      
          $('.slick-slide').on('click', function() {
            const slideIndex = $(this).attr('data-slick-index');
            const thisPhotoSrc = $(this).find('.portfolio_card__carusel-item img').attr('src');
      
            $(this).parents('.portfolio_card__carusel').slick('slickGoTo', slideIndex);
            $(this).parents('.portfolio_card').find('.portfolio_card__pic img').attr( 'src', thisPhotoSrc );
      
          });

          _thisCarusel.attr('data-slider', 'true');
        } else {
          $('.portfolio_card__carusel-item').on('click', function() {
            const thisPhotoSrc = $(this).find('img').attr('src');
            $(this).parents('.portfolio_card').find('.portfolio_card__pic img').attr( 'src', thisPhotoSrc );
          });
        }
    
      });
    });
  });

}

export default ourRepairsBlock;