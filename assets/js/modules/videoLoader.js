const videoLoader = (elem) => {

  $(elem).on('click', function() {
    const videoID    = $(this).attr('data-video');
    const videoFrame = `<iframe src="https://www.youtube.com/embed/${videoID}?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
    $(this).append(videoFrame);
    $(this).addClass('opacity');
    $(this).nextAll().fadeOut();
   });

}

export default videoLoader;