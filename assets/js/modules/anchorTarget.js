const anchorTarget = (elem) => {

  $(elem).on('click', function(event) {
  
    const anchor = $(this).attr('href');
    const target = $(anchor);

    if ( anchor.indexOf('#') === 0 ) {
      if ( event !== undefined ) {
        event.preventDefault();
      } else {
        console.log( 'If the tag is <a>, add the event to the second property' );
      }
      
      let targetVal = target.offset().top;
      if ( target.offset().top > 100 ) {
        targetVal = targetVal -99;
      }

      $('html').animate({
        'scrollTop': targetVal
      }, 1000, 'swing');
  
      return;
    }
  });
	
}

export default anchorTarget;