import sendData from './sendData.js';
import { setCookie } from './cookie.js';

const validate = function() {

    const _thisForm = $(this).parents('form');
    const pageID = $('body').attr('id').slice(5);

    // Добавляем метод для проверки имени
    // $.validator.addMethod('lettersonly', function(value, element) {
    // 	return this.optional(element) || /^[a-z, " ", а-я ]+$/i.test(value);
    //  }, 'Доступны только буквы для ввода');

    $(_thisForm).validate({
        errorClass: 'input-error',
        rules: {
            ima_kl: {
                required: false,
                minlength: 2,
                // lettersonly: true
            },
            telefon_lida: {
                required: true,
                // minlength: 10
            },
            date: {
                required: true,
                // minlength: 10,
            },
            time: {
                required: true,
                // minlength: 11,
            },
            address: {
                required: true
            }
        },
        messages: {
            ima_kl: {
                required: '',
                minlength: ''
            },
            telefon_lida: {
                required: '',
                minlength: ''
            },
            date: {
                required: '',
                minlength: ''
            },
            time: {
                required: '',
                minlength: ''
            },
            address: {
                required: '',
                minlength: ''
            }
        },
        submitHandler: function() {
            console.log('ghghgh');
            if (_thisForm.valid()) {
                _thisForm.find('[type=submit]').prop('disabled', true);
                const formID = _thisForm.find('input[name="site_form_id"]').val();
                if (formID !== undefined) {
                    sendData('form', 'send', formID, 'false');
                }
                $.ajax({
                    type: 'POST',
                    url: '/wp-content/themes/flagman/mail.php',
                    data: _thisForm.serialize(),
                    success: function(res) {
                        if (pageID === 'success') {
                            $('.firstscreen .section-board_title').text('Спасибо! Мы свяжемся с вами в ближайшее время');
                            $('.firstscreen .section-board_form').slideUp();
                        } else {
                            setCookie('home_page_link', window.location.href);

                            let redirectPage = '/success';
                            if (pageID === 'detail') {
                                redirectPage = '/success-detail';
                            }
                            window.location.href = redirectPage;
                        }
                        // _thisForm.find('[type=submit]').prop( 'disabled', false );
                    }
                });
            }
        }
    });
}

export default validate;