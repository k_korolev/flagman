const ajaxContentLoader = (action, param, success, ...customAjaxOptions) => {

  let options = {
    type: 'POST',
    url: '/wp-admin/admin-ajax.php',
    data: 'action=' + action + '&' + param,
    success: success
  }

  
  if ( customAjaxOptions.length > 0 ) {

    for ( let key in customAjaxOptions[0] ) {
      options[key] = customAjaxOptions[0][key];    
    }
    
  }

  $.ajax(options);
}

export default ajaxContentLoader;