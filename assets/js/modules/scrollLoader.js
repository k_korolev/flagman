import ajaxContentLoader from './ajaxContentLoader.js';

const scrollLoader = (options) => {

  const { blockClass, ajaxLoader, action, param, handler } = options;
  
  if ( $(blockClass).length > 0 ) {
    
    const prevBlockOffset = $(blockClass).prev().prev().offset().top;
    let contentLoad = false;

    if ( ajaxLoader === true ) {
      $(window).on('scroll', () => {
        if ( contentLoad === false && $(window).scrollTop() > prevBlockOffset ) {
          ajaxContentLoader( action, 'param='+param, handler );
          contentLoad = true;
        }
      });
    } else {
      $(window).on('scroll', () => {
        if ( contentLoad === false && $(window).scrollTop() > prevBlockOffset ) {
          handler;
          contentLoad = true;
        }
      });
    }
    
  }

}

export default scrollLoader;