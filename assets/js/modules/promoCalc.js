const userCalcData = (param) => {
  
  // Собираем все данные в объект
  $('.section-board_card-wrap').each(function() {
    const item = $(this).find('input[type="radio"]:checked, input[type="number"], select');
    const itemName  = item.attr('name');
    const itemTitle = item.parents('.section-board_card').find('.section-board_card__title').text();
    let itemValue;
    if ( item.hasClass('section-board_card__radio') ) {
      itemValue = item.next().text();
    } else {
      itemValue = item.val();
    }

    if ( itemName !== undefined ) {
      param[itemName] = {title: itemTitle.slice(0,-1), value: itemValue};
    }

  });

  // Собираем комментарий в АМО
  let amoMSG = 'Клиент ввел в калькулятор следующие данные:\n';
  for ( let val in param) {
    if ( typeof(param[val]) === 'object' ) {
      amoMSG += `\n${param[val]['title']}: ${param[val]['value']}`;
    }    
  }
  $('#modal_promo input[name="comment"]').val(amoMSG);

  // Сообщение в модалке
  const repairObj = {
    'Косметический': 'косметического ремонта',
    'Капитальный'  : 'капитального ремонта',
    'Под ключ'     : 'ремонта под ключ'
  };
  let repairType;
  if ( param['promo-repair'] !== undefined ) {
    repairType = repairObj[param['promo-repair']['value']];
  } else {
    repairType = 'ремонта';
  }

  const roomObj = {
    '0': 'студии',
    '1': '1 комнатной квартире',
    '2': '2-х комнатной квартире',
    '3': '3-х комнатной квартире',
    '4': '4-х комнатной квартире',
    '5': 'квартире'
  };
  const roomNum = roomObj[param['promo-room']['value']];

  const modalDESC = `Сметчик рассчитает примерную стоимость ${repairType} в вашей ${roomNum} площадью ${param['promo-area']['value']}м²`;
  $('#modal_promo .modal_info__title').text(modalDESC);

}

export default userCalcData;