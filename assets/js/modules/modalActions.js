import ajaxContentLoader from './ajaxContentLoader.js';
import userCalcData from './promoCalc.js';

const modalOpenAjax = function() { 
  const modal    = $('.modal-ajax');
  const _this    = $(this);
  const target   = _this.attr('data-target');
  const paramObj = {};

  if ( _this.attr('data-formid') !== undefined ) {
    paramObj['formid'] = _this.attr('data-formid');
  }

  if ( _this.hasClass('pricing_card__toggle') ) {
    paramObj['pricing_id'] = _this.parents('.pricing_card').attr('data-id');
  }

  const param = $.param(paramObj);
  
  modal.fadeIn().addClass('show').attr('id', target);

  ajaxContentLoader( target, param, (response) => {
    $('.modal-ajax .modal-container').html(response)
    $('body').addClass('modal-open');
  });
  
}

const modalOpen = function() {

  const _this    = $(this);
  const target   = _this.attr('data-target');
  const formID   = _this.attr('data-formid');
  const paramObj = {};
  userCalcData(paramObj);  

  $(`#${target}`).addClass('show').fadeIn()
    .find('input[name="site_form_id"]').val(formID);

}

const modalClose = function() {

  const _this = $(this);
  const modal = _this.parents('.modal');

  $('body').removeClass('modal-open');
  modal.removeClass('show').fadeOut();
  if ( modal.hasClass('modal-ajax') ) {
    _this.parent().remove();
  }

}

export { modalOpenAjax, modalOpen, modalClose }