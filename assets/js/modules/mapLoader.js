const mapLoader = () => {
  const prevBlockOffset = $('.contacts').prev().prev().offset().top;
  let contentLoad = false;
  $(window).on('scroll', () => {
    if ( contentLoad === false && $(window).scrollTop() > prevBlockOffset ) {
      const mapFrame = '<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A0f9a71c9c8dd8cdad2eb4e88a3e2441ca6577bf65e920c943cfd94c93d48103e&amp;source=constructor" width="100%" height="100%" scrolling="no" frameborder="0"></iframe>';
      $('.contacts-map').html(mapFrame);
      contentLoad = true;
    }
  });
}

export default mapLoader;