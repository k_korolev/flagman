<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package flagman
 */

get_header(); ?>
	
	<main class="main-404">
		<div class="container-404 d-flex flex-column justify-content-center align-items-center">
			<p class="text-404 position-relative">страница</p>
			<p class="content-404">404</p>
			<a href="<?php echo home_url(); ?>" class="btn-404 btn">Вернуться на главную</a>
		</div>
	</main>
