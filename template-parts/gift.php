<section class="gift">
  <div class="container d-lg-flex justify-content-lg-center">
    <?php $upload_dir = wp_upload_dir(); ?>
    <div class="gift_pic">
      <picture class="gift_pic-wrap"><img data-src="<? echo $upload_dir['baseurl']; ?>/gift-pic.png" alt="gift" class="lazyload"></picture>
    </div>
    <div class="gift-wrap">
      <h2 class="section-title">Ну что, начнём ремонт с подарка? :)</h2>
      <p class="gift_desc">
        Дарим сертификат на 5000 рублей в строительном торговом доме «Петрович»! 
        Подарок будет закреплён за вами после составления сметы на ремонт.
      </p>
      <div class="section-board">
        <form action="" class="section-board_form d-xl-flex flex-xl-wrap">
          <input type="hidden" name="site_form_id" value="podarok_send">
          <? get_template_part( 'assets/inc/analitics' ); ?>
          <input type="tel" name="telefon_lida" placeholder="Ваш телефон">
          <p class="section-board_form__agree">Я даю своё согласие на обработку персональных данных</p>
          <input type="submit" value="Узнать стоимость" class="btn">
        </form>
      </div>
    </div>
  </div>
</section>