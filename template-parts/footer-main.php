<?php get_template_part( 'template-parts/btn-to-top' ); ?>
<footer class="footer">
  <div class="container d-md-flex flex-md-wrap justify-content-md-between">
    <?php $upload_dir = wp_upload_dir(); ?>
    <a href="<?php echo home_url(); ?>" class="footer_logo">
      <img data-src="<?php echo $upload_dir['baseurl']; ?>/logo-footer.png" alt="Logo" class="lazyload">
    </a>
    <?php
      if ( is_front_page() ) :
        echo '<nav class="footer_nav">';
          echo wp_nav_menu(
            array(
              'theme_location' => 'footer',
              'container'      => 'ul',
              'menu_class'     => 'footer_nav__list d-md-flex justify-content-md-center align-items-md-center',
              'echo'           => false
            )
          );
        echo '</nav>';
      endif;
    ?>
    <ul class="footer_social d-flex align-items-md-center">
      <li class="footer_social__item"><a href="https://business.facebook.com/flagmanremonta/?roistat_visit=258464" class="footer_social__item-fb"></a></li>
      <li class="footer_social__item"><a href="https://www.instagram.com/flagmanremontaspb/?roistat_visit=258464" class="footer_social__item-inst"></a></li>
    </ul>
    <p class="footer_copy">
      <span>© 2014-<?php echo date("Y"); ?> «Флагман Ремонта». Все права защищены. </span>
      <a href="<?php echo home_url( '/privacy-policy' ); ?>">Политика конфиденциальности</a>
    </p>
  </div>
</footer>