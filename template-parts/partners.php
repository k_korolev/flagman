<section class="partners">
  <div class="container">
    <h2 class="section-title">Позаботимся о материалах</h2>
    <p class="partners_desc">
      Мы напрямую сотрудничаем со многими торговыми компаниями и производителями.
      Это даёт возможность приобретать материалы по оптовым ценам и уникальным спецпредложениям.
    </p>
    <div class="partners-content d-xl-flex flex-xl-wrap justify-content-xl-center"></div>
  </div>
</section>