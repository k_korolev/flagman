<header class="header">
  <div class="container">
    <p class="header-top"><span>Работаем с 2009 г.</span></p>
    <div class="d-flex justify-content-between align-items-center">
      <?php the_custom_logo(); ?>
      <a href="tel:+78123726932" class="header_phone d-flex justify-content-center align-items-center d-xl-none hmc_tel"><i></i></a>
      <button class="header_burger d-flex flex-column justify-content-around d-xl-none">
        <span class="header_burger__line header_burger__line-1"></span>
        <span class="header_burger__line header_burger__line-2"></span>
        <span class="header_burger__line header_burger__line-3"></span>
      </button>
      <nav class="header_nav d-xl-flex justify-content-xl-between">
        <?php echo wp_nav_menu(
          array(
            'theme_location' => 'header',
            'container'      => 'ul',
            'menu_class'     => 'header_nav__list d-xl-flex justify-content-xl-center align-items-xl-end',
            'echo'           => false
          )
        ); ?>
        <div class="d-xxl-flex">
          <article class="header_nav__contacts d-xl-flex flex-xl-column-reverse justify-content-xl-center">
            <a href="tel:+78123726932" class="header_nav__contacts-tel hnc_info__tel">+ 7 (812) 372-69-32</a>
            <p class="header_nav__contacts-time">Ежедневно с 08:00 до 22:00</p>
          </article>
          <button class="header_nav__btn btn" 
            data-toggle="modal-open" data-target="modal_call_header" 
            data-formid="menu.knopka-zakazat-zvonok_send">Заказать звонок</button>
        </div>
      </nav>
    </div>
  </div>
</header>