<section class="firstscreen">
  <div class="container d-md-flex justify-content-md-center">
    <div class="firstscreen-content">
      <h1 class="section-title">Мы получили вашу заявку!</h1>
      <p class="firstscreen_desc">Наш специалист свяжется с&nbsp;вами в&nbsp;течение 10&nbsp;минут</p>
      <div class="firstscreen_list">
        <p class="firstscreen_list__title d-none d-xl-block">Точный расчет стоимости ремонта</p>
        <ul class="firstscreen_list__wrap">
          <li>Невозможно рассчитать стоимость ремонта по телефону</li>
          <li>Выезд замерщика — это бесплатно и ни к чему вас не обязывает</li>
          <li>Только выезд замерщика на объект позволяет составить точную смету</li>
          <li>Замерщик покажет вам образцы и проконсультирует по всем вопросам</li>
        </ul>
      </div>
    </div>
    <div class="firstscreen-form">
      <div class="section-board">
        <h3 class="section-board_title">Когда вам удобно принять нашего замерщика?</h3>
        <form action="" class="section-board_form">
          <input type="text" name="date" placeholder="Укажите дату" readonly>
          <div class="section-board_time-wrap">
            <input type="text" name="time" placeholder="Укажите время" readonly>
            <ul class="section-board_time__list">
              <li>10.00-13.00</li>
              <li>14.00-17.00</li>
              <li>18.00-21.00</li>
            </ul>
          </div>
          <input type="text" name="address" placeholder="Укажите адрес">
          <p class="section-board_form__agree">Я даю своё согласие на обработку персональных данных.</p>
          <input type="submit" value="Вызвать замерщика" class="btn">
        </form>
      </div>
    </div>
  </div>
  <?php get_template_part( 'template-parts/wave_promo' ); ?>
</section>