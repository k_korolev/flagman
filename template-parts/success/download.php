<section class="download">
  <div class="container d-md-flex justify-content-md-center">
    <?php $upload_dir = wp_upload_dir(); ?>
    <picture class="download_pic">
      <img data-src="<? echo $upload_dir['baseurl']; ?>/flagman-munal.png" alt="Image" class="lazyload">
    </picture>
    <div class="download-content">
      <h2 class="section-title">Памятка по ремонту</h2>
      <p class="download_desc">Как сделать ремонт, сохранив нервы и не оставшись обманутым</p>
      <a href="https://flagmanremonta.spb.ru/doc/booklet.pdf" class="btn" download>Скачать памятку в .pdf</a>
    </div>
  </div>
</section>