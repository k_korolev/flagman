<?php 
  $upload_dir = wp_upload_dir();
  $stars_dir  = $upload_dir['baseurl'] . '/reviews-stars.png';
?>
<section class="otzovik">
  <div class="container d-sm-flex flex-sm-wrap justify-content-sm-center align-items-sm-end">
    <div class="otzovik_card">
      <picture class="otzovik_card__pic otzovik_card__pic-yell"><img src="<? echo $upload_dir['baseurl']; ?>/otzovik-yell.png" alt="yell.ru"></picture>
      <div class="otzovik_card__rate">
        <div class="otzovik_card__rate-stars"><img src="<?=$stars_dir?>" alt="Image"></div>
        <span class="otzovik_card__rate-rank">4.8</span>
      </div>
    </div>
    <div class="otzovik_card">
      <picture class="otzovik_card__pic otzovik_card__pic-apoi"><img src="<? echo $upload_dir['baseurl']; ?>/otzovik-apoi.png" alt="apoi"></picture>
      <div class="otzovik_card__rate">
        <div class="otzovik_card__rate-stars"><img src="<?=$stars_dir?>" alt="Image"></div>
        <span class="otzovik_card__rate-rank">4.8</span>
      </div>
    </div>
    <div class="otzovik_card">
      <picture class="otzovik_card__pic otzovik_card__pic-pro"><img src="<? echo $upload_dir['baseurl']; ?>/otzovik-pro.png" alt="otzyvy.pro"></picture>
      <div class="otzovik_card__rate">
        <div class="otzovik_card__rate-stars"><img src="<?=$stars_dir?>" alt="Image"></div>
        <span class="otzovik_card__rate-rank">4.8</span>
      </div>
    </div>
    <div class="otzovik_card">
      <picture class="otzovik_card__pic otzovik_card__pic-otzyv"><img src="<? echo $upload_dir['baseurl']; ?>/otzovik-otzyv.png" alt="otzyv.ru"></picture>
      <div class="otzovik_card__rate">
        <div class="otzovik_card__rate-stars"><img src="<?=$stars_dir?>" alt="Image"></div>
        <span class="otzovik_card__rate-rank">4.8</span>
      </div>
    </div>
  </div>
</section>