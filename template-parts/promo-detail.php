<section class="promo">
  <div class="container d-md-flex flex-md-wrap justify-content-md-center">
    <h1 class="section-title">Ремонт квартир в Санкт-Петербурге</h1>
    <div class="promo_calc">
      <div class="section-board">
        <h3 class="section-board_title">Поможем рассчитать стоимость</h3>
        <div class="section-board_form">
          <div class="section-board_form-wrapper">
            <div class="section-board_form-scroller d-xl-flex flex-xl-wrap justify-content-xl-center">
              <div class="section-board_card">
                <p class="section-board_card__title">1. Тип жилья?</p>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-object" id="promo-new" class="section-board_card__radio" checked>
                  <label for="promo-new" class="section-board_card__label">Новостройка</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-object" id="promo-old" class="section-board_card__radio">
                  <label for="promo-old" class="section-board_card__label">Вторичка</label>
                </div>
              </div>
              <div class="section-board_card section-board_card-area">
                <p class="section-board_card__title">2. Введите площадь:</p>
                <div class="section-board_card-wrap">
                  <input type="number" name="promo-area" id="promo-area" class="section-board_card__area" min="10" max="250" value="40">
                  <span>м²</span>
                </div>
              </div>
              <div class="section-board_card">
                <p class="section-board_card__title">3. Cколько комнат?</p>
                <div class="section-board_card-wrap">
                  <select name="promo-room" class="section-board_card__select">
                    <option value="1">1 комнатная</option>
                    <option value="2">2 комнатная</option>
                    <option value="3">3 комнатная</option>
                    <option value="4">4 комнатная</option>
                    <option value="0">Квартира-студия</option>
                    <option value="5">Другая</option>
                  </select>
                </div>
              </div>
              <div class="section-board_card">
                <p class="section-board_card__title">4. В каком состоянии находится квартира?</p>
                <div class="section-board_card-wrap">
                  <select name="promo-condition" class="section-board_card__select">
                    <option value="Сдана без отделки">Сдана без отделки</option>
                    <option value="Сдана с черновой отделкой">Сдана с черновой отделкой</option>
                    <option value="Сдана с предчистовой отделкой (White Box)">Сдана с предчистовой отделкой (White Box)</option>
                    <option value="Не знаю (сложно сказать)">Не знаю (сложно сказать)</option>
                  </select>
                </div>
              </div>
              <div class="section-board_card">
                <p class="section-board_card__title">5. Нужна ли перепланировка?</p>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-redevelopment" id="promo-redevelopment-yes" class="section-board_card__radio">
                  <label for="promo-redevelopment-yes" class="section-board_card__label">Да</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-redevelopment" id="promo-redevelopment-partial" class="section-board_card__radio" checked>
                  <label for="promo-redevelopment-partial" class="section-board_card__label">Частичная</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-redevelopment" id="promo-redevelopment-no" class="section-board_card__radio">
                  <label for="promo-redevelopment-no" class="section-board_card__label">Нет</label>
                </div>
              </div>
              <div class="section-board_card">
                <p class="section-board_card__title">6. Нужно ли выравнивать полы?</p>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-floors" id="promo-floors-1" class="section-board_card__radio" checked>
                  <label for="promo-floors-1" class="section-board_card__label">Да</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-floors" id="promo-floors-2" class="section-board_card__radio">
                  <label for="promo-floors-2" class="section-board_card__label">Нет</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-floors" id="promo-floors-3" class="section-board_card__radio">
                  <label for="promo-floors-3" class="section-board_card__label">Не знаю</label>
                </div>
              </div>
              <div class="section-board_card">
                <p class="section-board_card__title">7. Нужно ли выравнивать стены?</p>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-walls" id="promo-walls-1" class="section-board_card__radio" checked>
                  <label for="promo-walls-1" class="section-board_card__label">Да</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-walls" id="promo-walls-2" class="section-board_card__radio">
                  <label for="promo-walls-2" class="section-board_card__label">Нет</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-walls" id="promo-walls-3" class="section-board_card__radio">
                  <label for="promo-walls-3" class="section-board_card__label">Не знаю</label>
                </div>
              </div>
              <div class="section-board_card">
                <p class="section-board_card__title">8. Будете ли менять проводку?</p>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-wiring" id="promo-wiring-1" class="section-board_card__radio" checked>
                  <label for="promo-wiring-1" class="section-board_card__label">Да</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-wiring" id="promo-wiring-2" class="section-board_card__radio">
                  <label for="promo-wiring-2" class="section-board_card__label">Нет</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-wiring" id="promo-wiring-3" class="section-board_card__radio">
                  <label for="promo-wiring-3" class="section-board_card__label">Не знаю</label>
                </div>
              </div>
              <div class="section-board_card">
                <p class="section-board_card__title">9. Какой планируете делать потолок?</p>
                <div class="section-board_card-wrap">
                  <select name="promo-ceiling" class="section-board_card__select">
                    <option value="Покраска">Покраска</option>
                    <option value="Натяжной">Натяжной</option>
                    <option value="Многоуровневый">Многоуровневый</option>
                    <option value="Не знаю">Не знаю</option>
                  </select>
                </div>
              </div>
              <div class="section-board_card">
                <p class="section-board_card__title">10. Где находится квартира?</p>
                <div class="section-board_card-wrap">
                  <select name="promo-location" class="section-board_card__select">
                    <option value="В пределах КАД">В пределах КАД</option>
                    <option value="до 20 км за КАД">до 20 км за КАД</option>
                    <option value="более 20 км за КАД">более 20 км за КАД</option>
                  </select>
                </div>
              </div>
              <div class="section-board_card">
                <p class="section-board_card__title">11. Нужнали помощь в закупке материалов от компании?</p>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-help" id="promo-help-1" class="section-board_card__radio" checked>
                  <label for="promo-help-1" class="section-board_card__label">Да</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-help" id="promo-help-2" class="section-board_card__radio">
                  <label for="promo-help-2" class="section-board_card__label">Нет</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-help" id="promo-help-3" class="section-board_card__radio">
                  <label for="promo-help-3" class="section-board_card__label">Не знаю</label>
                </div>
              </div>
              <div class="section-board_card">
                <p class="section-board_card__title">12. Когда планируете делать ремонт?</p>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-date" id="promo-date-1" class="section-board_card__radio" checked>
                  <label for="promo-date-1" class="section-board_card__label">Чем раньше, тем лучше</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-date" id="promo-date-2" class="section-board_card__radio">
                  <label for="promo-date-2" class="section-board_card__label">В ближайшие 2 месяца</label>
                </div>
                <div class="section-board_card-wrap">
                  <input type="radio" name="promo-date" id="promo-date-3" class="section-board_card__radio">
                  <label for="promo-date-3" class="section-board_card__label">Только хочу узнать цену</label>
                </div>
              </div>
            </div>
          </div>
          <button class="calc_btn btn" 
            data-toggle="modal-open" 
            data-target="modal_promo" 
            data-formid="1ekran.kalkulyator-knopka-uznat-stoimost_send">Узнать стоимость</button>
        </div>
      </div>
    </div>
    <ul class="promo_desc">
      <li class="promo_desc__item">
        <i class="promo_desc__item-icon promo_desc__item-icon_01"></i>
        <p class="promo_desc__item-text">Гарантия 3&nbsp;года на&nbsp;все&nbsp;работы</p>
      </li>
      <li class="promo_desc__item">
        <i class="promo_desc__item-icon promo_desc__item-icon_02"></i>
        <p class="promo_desc__item-text">Без авансов, с&nbsp;поэтапной оплатой</p>
      </li>
      <li class="promo_desc__item">
        <i class="promo_desc__item-icon promo_desc__item-icon_03"></i>
        <p class="promo_desc__item-text">Фиксируем в договоре стоимость и&nbsp;сроки</p>
      </li>
    </ul>
  </div>
  <?php get_template_part( 'template-parts/wave_promo' ); ?>
</section>