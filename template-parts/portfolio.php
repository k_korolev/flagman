<section class="portfolio" id="our-repairs">
  <div class="container">
    <h2 class="section-title">Наши работы</h2>
    <p class="portfolio_desc">
      <span>Качество и внимание к деталям —</span> наша главная ценность. 
      Все мастера проходят обязательную аттестацию на специальном учебном полигоне. 
      По вашему желанию организуем приёмку работ независимой экспертизой.
    </p>
    <?php

      $user_device    = user_device();
      $category_args  = array( 'taxonomy' => 'our_repairs_category' );
      $category_items = get_terms( $category_args );
      
      $cont_tag   = 'ul';
      $child_tag  = 'li';
      $cont_class = 'portfolio-nav btn-list';
      $child_attr = 'data-value';

      if( $user_device == 'mobile' ) :
        $cont_tag   = 'select';
        $child_tag  = 'option';
        $cont_class = 'portfolio-nav btn-select';
        $child_attr = 'value';
      endif;
      
      echo '<' . $cont_tag . ' class="' . $cont_class . '">';
        foreach( $category_items as $item ) :
          echo '<' . $child_tag . ' '. $child_attr . '="' . $item->slug . '">' . $item->name . '</' . $child_tag . '>';
        endforeach;
      echo '</' . $cont_tag . '>';

      $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
      $max_page = wp_count_posts( 'our_repairs' );

    ?>
    <div class="portfolio-wrap">      
      <button class="btn-transparent" data-toggle="load_more">Показать больше</button>
    </div>
  </div>
</section>