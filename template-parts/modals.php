<div class="modal" id="modal_call">
  <div class="modal-container">
    <div class="modal-content">
      <button class="modal_close" data-toggle="modal-close"></button>
      <div class="modal-body">
        <h3 class="modal_title">Мы сориентируем вас по цене ремонта и проконсультируем по любым вопросам</h3>
        <form action="#" class="modal_form">
          <input type="hidden" name="site_form_id" value="">
          <? get_template_part( 'assets/inc/analitics' ); ?>
          <div class="modal_form-body">
            <input type="text" name="ima_kl" placeholder="Ваше имя" class="modal_form__input">
            <input type="tel" name="telefon_lida" placeholder="Ваш телефон" autocomplete="off" class="modal_form__input">
          </div>
          <p class="modal_form__consent">Я даю свое согласие на обработку персональных&nbsp;данных</p>
          <input type="submit" value="Заказать звонок" class="btn" data-target="form-submit">
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_call_header">
  <div class="modal-container">
    <div class="modal-content">
      <button class="modal_close" data-toggle="modal-close"></button>
      <div class="modal-body">
        <h3 class="modal_title">Мы сориентируем вас по цене ремонта и проконсультируем по любым вопросам</h3>
        <form action="#" class="modal_form">
          <input type="hidden" name="site_form_id" value="">
          <? get_template_part( 'assets/inc/analitics' ); ?>
          <div class="modal_form-body">
            <input type="tel" name="telefon_lida" placeholder="Ваш телефон" autocomplete="off" class="modal_form__input">
          </div>
          <p class="modal_form__consent">Я даю свое согласие на обработку персональных&nbsp;данных</p>
          <input type="submit" value="Заказать звонок" class="btn" data-target="form-submit">
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_promo">
  <div class="modal-container">
    <div class="modal-content">
      <button class="modal_close" data-toggle="modal-close"></button>
      <div class="modal-body">
        <h3 class="modal_title">Ваши данные отправлены сметчику!</h3>
        <article class="modal_info">
          <p class="modal_info__title">
            Сметчик рассчитает примерную стоимость <span class="calc_repair"></span> 
            в&nbsp;вашей <span class="calc_room"></span> площадью <span class="calc_area"></span>&nbsp;м²
          </p>
          <p class="modal_info__subtitle">Оставьте свой номер, чтобы узнать&nbsp;цену.</p>
        </article>
        <form action="#" class="modal_form">
          <input type="hidden" name="site_form_id" value="">
          <input type="hidden" name="comment" value="">
          <? get_template_part( 'assets/inc/analitics' ); ?>
          <div class="modal_form-body">
            <input type="tel" name="telefon_lida" placeholder="Ваш телефон" autocomplete="off" class="modal_form__input">
          </div>
          <p class="modal_form__consent">Я даю свое согласие на обработку персональных&nbsp;данных</p>
          <input type="submit" value="Заказать звонок" class="btn" data-target="form-submit">
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal modal-ajax"><div class="modal-container"></div></div>