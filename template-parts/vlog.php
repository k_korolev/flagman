<section class="vlog">
  <div class="container">
    <h2 class="section-title">Показываем, как мы делаем ремонт</h2>
    <div class="vlog-content d-md-flex flex-md-wrap justify-content-md-center">
      <?php
        $vlog_args = [
          'numberposts' => 4,
          'post_type'   => 'vlog',
          'order'       => 'ASC',
          'orderby'     => 'title',
          'suppress_filters' => true
        ];
        $vlog_posts = get_posts( $vlog_args );
        foreach ( $vlog_posts as $post ) :

          setup_postdata( $post );
          
          $vlog_title    = get_field( 'vlog_title' );
          $vlog_subtitle = get_field( 'vlog_subtitle' );
          $vlog_id  = get_field( 'vlog_id' );
          $vlog_pic = get_field( 'vlog_pic' ); ?>

          <div class="vlog_card">
            <div class="vlog_card__pic" data-video="<?=$vlog_id?>">
              <img data-src="<?=$vlog_pic?>" alt="<?=$vlog_title?>" class="lazyload">
              <span class="icon-play"><i></i></span>
            </div>
            <span class="vlog_card__icon"></span>
            <article class="vlog_card-info">
              <p class="vlog_card__subtitle"><?=$vlog_subtitle?></p>
              <p class="vlog_card__title"><?=$vlog_title?></p>
            </article>
          </div>

        <? endforeach;
        wp_reset_postdata();
      ?>
    </div>
  </div>
  <?php get_template_part( 'template-parts/wave_block' ); ?>
</section>