<?php $upload_dir = wp_upload_dir(); ?>
<section class="benefit">
  <?php get_template_part( 'template-parts/wave_lines' ); ?>
  <div class="container">
    <h2 class="section-title">Почему у нас выгоднее, чем у частных мастеров</h2>
    <select class="benefit_select btn-select">
      <option value="benefit-flagman" selected>Флагман ремонта</option>
      <option value="benefit-other">Частные мастера</option>
    </select>
    <div class="benefit-content d-lg-flex justify-content-lg-center">
      <div class="benefit_card" data-target="benefit-other">
        <div class="benefit_card-top">
          <picture class="benefit_card__pic"><img data-src="<? echo $upload_dir['baseurl']; ?>/benefit-other.jpg" alt="img" class="lazyload"></picture>
          <p class="benefit_card__title">Частные мастера</p>
        </div>
        <ul class="benefit_card__list">
          <li class="benefit_card__list-item">
            <p class="benefit_card__list-item_title">Прикидывают приблизительную стоимость и при каждом удобном случае меняют её</p>
            <p class="benefit_card__list-item_desc">
              Часто рассчитывают цену «из метража», даже не видя вашей квартиры. 
              Ни вы, ни мастер не можете точно сказать, что входит в стоимость. 
              Электрика и сантехника в последний момент оказываются оплачиваемыми 
              по отдельной цене с аргументом, что «все так делают». Цена, которая 
              потом может увеличиться до 2 раз
            </p>
          </li>
          <li class="benefit_card__list-item">
            <p class="benefit_card__list-item_title">Работают по мере способностей и без контроля</p>
            <p class="benefit_card__list-item_desc">
              Строители-самоучки ничего не знают о СНиП и просто делают, как им нравится. 
              Игнорируют важные технологические моменты, не дают материалам просохнуть, 
              спешат получить от вас деньги.
            </p>
          </li>
          <li class="benefit_card__list-item">
            <p class="benefit_card__list-item_title">Не успел ремонт закончиться, их уже нет</p>
            <p class="benefit_card__list-item_desc">
              Вы не найдёте мастеров после окончания ремонта. Если повезёт, 
              они помогут решить хотя бы те проблемы, что всплыли сразу. 
              За всё остальное придётся платить, причём другим мастерам.
            </p>
          </li>
          <li class="benefit_card__list-item">
            <p class="benefit_card__list-item_title">Постоянно просят что-то докупить, довезти, дорешать</p>
            <p class="benefit_card__list-item_desc">
              Частные мастера обычно совсем не умеют планировать работы. 
              Вам придётся держать всё под своим контролем, организовывать 
              поставки материалов и точно знать, что это не в последний раз.
            </p>
          </li>
          <li class="benefit_card__list-item">
            <p class="benefit_card__list-item_title">«Заплатите вперёд, нам нужно отправить денег семьям!»</p>
            <p class="benefit_card__list-item_desc">
              Частные мастера традиционно работают по предоплате и, увы, 
              нередко исчезают в самый неподходящий момент с вашими деньгами, оставив ремонт
            </p>
          </li>
        </ul>
      </div>
      <div class="benefit_card" data-target="benefit-flagman">
        <div class="benefit_card-top">
          <picture class="benefit_card__pic"><img data-src="<? echo $upload_dir['baseurl']; ?>/benefit-flagman.jpg" alt="img" class="lazyload"></picture>
          <p class="benefit_card__title">Флагман ремонта</p>
        </div>
        <ul class="benefit_card__list">
          <li class="benefit_card__list-item">
            <p class="benefit_card__list-item_title">Рассчитаем точную стоимость и закрепим её в договоре</p>
            <p class="benefit_card__list-item_desc">
              Составляем детальную смету по всем позициям. Стоимость сразу указываем в договоре. 
              Прораб на подписании договора подтверждает объём работ. После этого внести 
              корректировки в смету можете только вы!
            </p>
          </li>
          <li class="benefit_card__list-item">
            <p class="benefit_card__list-item_title">Работаем по совести и по жёстким стандартам</p>
            <p class="benefit_card__list-item_desc">
              Делаем всё строго в рамках сметы. Вы платите за каждый этап работ 
              только тогда, когда качество вас полностью устроило. 
              Технический надзор контролирует объекты.
            </p>
          </li>
          <li class="benefit_card__list-item">
            <p class="benefit_card__list-item_title">Даём гарантию три года по договору</p>
            <p class="benefit_card__list-item_desc">
              Если после ремонта возникнут какие-то проблемы (не более одного процента случаев), 
              мы всё исправим в течение недели с момента вашего обращения. В договоре это зафиксировано.
            </p>
          </li>
          <li class="benefit_card__list-item">
            <p class="benefit_card__list-item_title">Организованное производство и закупка материалов</p>
            <p class="benefit_card__list-item_desc">
              Прораб чётко рассчитает, сколько времени нужно на каждый этап работы, 
              и предоставит список всех необходимых материалов. Вам не придётся 
              внезапно ехать искать недостающие гвозди.
            </p>
          </li>
          <li class="benefit_card__list-item">
            <p class="benefit_card__list-item_title">Поэтапная оплата, ремонт - сначала</p>
            <p class="benefit_card__list-item_desc">
              Мы чётко обозначим в договоре, в какие сроки планируем выполнить 
              каждый этап работы. Принятый этап работы оплачивается в течение трёх дней, 
              а мы при этом переходим к следующему этапу, не дожидаясь поступления средств.
            </p>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>