<header class="header header-without-navigation">
  <div class="container">
    <p class="header-top"><span>Работаем с 2009 г.</span></p>
    <div class="d-flex justify-content-between align-items-center">
      <?php the_custom_logo(); ?>
      <a href="tel:+78123726932" class="header_phone d-flex justify-content-center align-items-center d-xl-none"><i></i></a>
      <div class="d-none d-xl-flex">
        <article class="header_nav__contacts d-xl-flex flex-xl-column-reverse justify-content-xl-center">
          <a href="tel:+78123726932" class="header_nav__contacts-tel">+ 7 (812) 372-69-32</a>
          <p class="header_nav__contacts-time">Ежедневно с 08:00 до 22:00</p>
        </article>
        <? if ( !is_page_template( 'page-success.php' ) ) : ?>
          <button class="header_nav__btn btn" 
            data-toggle="modal-open" data-target="modal_call_header" 
            data-formid="menu.knopka-zakazat-zvonok_send">Заказать звонок</button>
        <? endif; ?>
      </div>
    </div>
  </div>
</header>