<section class="pricing" id="pricing">
  <div class="container">
    <h2 class="section-title">Тарифы</h2>
    <div class="pricing-content d-xxl-flex justify-content-xxl-center align-items-xxl-center">
    <?php

      $args = array( 
        'post_type' => 'pricing',
        'order' => 'ASC',
        'suppress_filters' => true
      );

      $pricing_posts = get_posts($args);

      foreach($pricing_posts as $post) {
        setup_postdata( $post );
        $post_title = get_field( 'pricing_title' );
        $post_label = get_field( 'pricing_label' );
        $post_price = get_field( 'pricing_price' );
        $post_desc  = get_field( 'pricing_desc' );
        $post_id    = get_the_ID(); ?>

        <div class="pricing_card" data-label="<?=$post_label?>" data-id=<?=$post_id?>>
          <p class="pricing_card__title"><?=$post_title?></p>
          <p class="pricing_card__desc"><?=$post_desc?></p>
          <div class="pricing_card-body">
            <button class="pricing_card__toggle" 
              data-toggle="modal-open-ajax" 
              data-target="modal_pricing_info">Полный список работ</button>
            <p class="pricing_card__price">от <span><?=$post_price?></span> ₽/м²</p>
            <button class="pricing_card__btn btn" 
              data-toggle="modal-open" 
              data-target="modal_call" 
              data-formid="tarif.<?=$post_label?>-knopka-uznat-stoimost_send">Рассчитать для вашей квартиры</button>
          </div>
        </div> <?
      }
      wp_reset_postdata();

    ?>
    </div>
  </div>
  <?php get_template_part( 'template-parts/wave_block' ); ?>
</section>