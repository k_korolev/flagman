<?php

  $user_device = user_device();
  $team_nav    = get_field( 'page_team_category_nav' );

  if ( $team_nav ) : ?>
    <section class="our-team">
      <div class="container">
        <div class="our-team-top">
          <h2 class="section-title">Наша команда</h2>
          <?php
                      
            $cont_tag   = 'ul';
            $child_tag  = 'li';
            $cont_class = 'our-team_nav btn-list';
            $child_attr = 'data-value';

            if( $user_device == 'mobile' ) :
              $cont_tag   = 'select';
              $child_tag  = 'option';
              $cont_class = 'our-team_nav btn-select';
              $child_attr = 'value';
            endif;

            echo '<' . $cont_tag . ' class="' . $cont_class . '">';
              foreach( $team_nav as $item ) :
                echo '<' . $child_tag . ' '. $child_attr . '="' . $item->term_id . '">' . $item->name . '</' . $child_tag . '>';
              endforeach;
            echo '</' . $cont_tag . '>';

          ?>
          <p class="our-team_desc">
            Выслушают ваши пожелания и помогут спланировать ремонт так, чтобы вы остались довольны. 
            Составят точную смету, которая останется неизменной на протяжении всего ремонта.
          </p>
        </div>
        <div class="our-team-content"></div>
      </div>
      <?php get_template_part( 'template-parts/wave_block' ); ?>
    </section>
  <? endif;
?>