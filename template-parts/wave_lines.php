<picture class="wave_lines">
  <source media="(min-width: 768px)" srcset="<? echo get_template_directory_uri(); ?>/assets/img/bg_lines.png">
  <img src="<? echo get_template_directory_uri(); ?>/assets/img/bg_lines-s.png" alt="img" class="lazyload">
</picture>