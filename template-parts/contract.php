<section class="contract" id="contract">
  <?php get_template_part( 'template-parts/wave_lines' ); ?>
  <div class="container d-lg-flex justify-content-lg-centetr">
    <?php $upload_dir = wp_upload_dir(); ?>
    <picture class="contract_pic">
      <source media="(min-width: 1365px)" data-srcset="<? echo $upload_dir['baseurl']; ?>/contract_big.png">
      <img data-src="<? echo $upload_dir['baseurl']; ?>/contract.png" alt="img" class="lazyload">
    </picture>
    <div class="contract-content">
      <h2 class="section-title">Договор, с которым вы будете спокойны</h2>
      <ul class="contract_list">
        <li class="contract_list__item">
          <span>Стоимость</span> зафиксирована в договоре и не меняется. 
          Повлиять на неё можете только вы сами — если в процессе ремонта у вас 
          появятся новые пожелания. Тогда дополнительные работы мы согласуем 
          отдельно и включим их в наш договор.
        </li>
        <li class="contract_list__item">
          <span>Сроки</span> чётко прописаны в договоре и не нарушаются. 
          За каждый день просрочки мы возвращаем вам деньги.
        </li>
        <li class="contract_list__item">
          <span>Гарантия 3 года на все виды работ.</span> Мы бесплатно решим любые 
          возможные проблемы, возникшие в течение гарантийного срока после нашего ремонта.
        </li>
      </ul>
    </div>
  </div>
</section>