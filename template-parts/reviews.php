<section class="reviews" id="reviews">
  <div class="container">
    <h2 class="section-title">Отзывы клиентов</h2>
    <div class="reviews-content d-md-flex flex-md-wrap justify-content-md-center">
    <?php
      $reviews_args = array(
        'numberposts' => 4,
        'post_type'   => 'video_review',
        'order'       => 'ASC',
        'orderby'     => 'title',
        'suppress_filters' => true
      );
      $reviews_posts = get_posts( $reviews_args );
      foreach( $reviews_posts as $post ) :

        setup_postdata( $post );

        $post_ID     = $post->ID; 
        $video_id    = get_field( 'review_id', $post_ID );
        $video_scrin = get_field( 'review_pic', $post_ID ); ?>
        
        <div class="reviews_card">
          <div class="reviews_card__pic" data-video="<?=$video_id?>">
            <img data-src="<?=$video_scrin?>" alt="image" class="lazyload">
            <span class="icon-play"><i></i></span>
          </div>
        </div>
        
        <?

      endforeach;
    ?>
    </div>
  </div>
  <?php get_template_part( 'template-parts/wave_block' ); ?>
</section>