<section class="promo">
  <div class="container d-md-flex flex-md-wrap justify-content-md-center">
    <h1 class="section-title">Ремонт квартир в Санкт-Петербурге</h1>
    <div class="promo_calc">
      <div class="section-board">
        <h3 class="section-board_title">Поможем рассчитать стоимость</h3>
        <div class="section-board_form d-flex flex-wrap justify-content-center">
          <div class="section-board_card">
            <p class="section-board_card__title">Какой ремонт?</p>
            <div class="section-board_card-wrap">
              <input type="radio" name="promo-repair" id="promo-kosm" class="section-board_card__radio" checked>
              <label for="promo-kosm" class="section-board_card__label">Косметический</label>
            </div>
            <div class="section-board_card-wrap">
              <input type="radio" name="promo-repair" id="promo-kap" class="section-board_card__radio">
              <label for="promo-kap" class="section-board_card__label">Капитальный</label>
            </div>
            <div class="section-board_card-wrap">
              <input type="radio" name="promo-repair" id="promo-key" class="section-board_card__radio">
              <label for="promo-key" class="section-board_card__label">Под ключ</label>
            </div>
          </div>
          <div class="section-board_card">
            <p class="section-board_card__title">Тип жилья?</p>
            <div class="section-board_card-wrap">
              <input type="radio" name="promo-object" id="promo-new" class="section-board_card__radio" checked>
              <label for="promo-new" class="section-board_card__label">Новостройка</label>
            </div>
            <div class="section-board_card-wrap">
              <input type="radio" name="promo-object" id="promo-old" class="section-board_card__radio">
              <label for="promo-old" class="section-board_card__label">Вторичка</label>
            </div>
          </div>
          <div class="section-board_card section-board_card-area">
            <p class="section-board_card__title">Введите площадь:</p>
            <div class="section-board_card-wrap">
              <input type="number" name="promo-area" id="promo-area" class="section-board_card__area" min="10" max="250" value="40">
              <span>м²</span>
            </div>
          </div>
          <div class="section-board_card section-board_card-room">
            <p class="section-board_card__title">Cколько комнат?</p>
            <div class="section-board_card-wrap">
              <select name="promo-room" class="section-board_card__select">
                <option value="1">1 комнатная</option>
                <option value="2">2 комнатная</option>
                <option value="3">3 комнатная</option>
                <option value="4">4 комнатная</option>
                <option value="0">Квартира-студия</option>
                <option value="5">Другая</option>
              </select>
            </div>
          </div>
          <button class="calc_btn btn" 
            data-toggle="modal-open" 
            data-target="modal_promo" 
            data-formid="1ekran.kalkulyator-knopka-uznat-stoimost_send">Узнать стоимость</button>
        </div>
      </div>
    </div>
    <ul class="promo_desc">
      <li class="promo_desc__item">
        <i class="promo_desc__item-icon promo_desc__item-icon_01"></i>
        <p class="promo_desc__item-text">Гарантия 3&nbsp;года на&nbsp;все&nbsp;работы</p>
      </li>
      <li class="promo_desc__item">
        <i class="promo_desc__item-icon promo_desc__item-icon_02"></i>
        <p class="promo_desc__item-text">Без авансов, с&nbsp;поэтапной оплатой</p>
      </li>
      <li class="promo_desc__item">
        <i class="promo_desc__item-icon promo_desc__item-icon_03"></i>
        <p class="promo_desc__item-text">Фиксируем в договоре стоимость и&nbsp;сроки</p>
      </li>
    </ul>
  </div>
  <?php get_template_part( 'template-parts/wave_promo' ); ?>
</section>