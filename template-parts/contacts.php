<section class="contacts" id="contacts">
  <?php get_template_part( 'template-parts/wave_lines' ); ?>
  <div class="container d-xl-flex justify-content-xl-between align-items-xl-center">
    <article class="contacts-info">
      <h2 class="section-title">Контакты</h2>
      <ul class="contacts_list">
        <li class="contacts_list__item">
          <a href="tel:+78123726932" class="contacts_list__item-title contacts-info-list_item__title">+ 7 (812) 372-69-32</a>
          <p class="contacts_list__item-subtitle">с 8:00 до 22:00, без выходных</p>
        </li>
        <li class="contacts_list__item">
          <p class="contacts_list__item-title">info@flagmanremonta.spb.ru</p>
          <p class="contacts_list__item-subtitle">Пишите на почту по любым вопросам</p>
        </li>
        <li class="contacts_list__item">
          <p class="contacts_list__item-title">г. Санкт-Петербург, ул. Комсомола, д. 41, оф. 527 (БЦ “Финляндский”)</p>
          <p class="contacts_list__item-subtitle">Адрес нашего офиса</p>
        </li>
      </ul>
    </article>
    <div class="contacts-map"></div>
  </div>
</section>